from locust import HttpUser, TaskSet, task, between
from faker import Faker

import random
import json

global entities
global fake

def index_random(l):
    headers = {'content-type': 'application/json'}
    l.client.get("/", headers=headers)

def index(l):
    headers = {'content-type': 'application/json'}
    l.client.get("/recipes/index", headers=headers)

def create(l):
    fake = Faker('es_ES')
    rand_entity = 'recipes'
    headers = {'content-type': 'application/json'}
    l.client.post("/" + rand_entity + "/create",
        data=json.dumps({
            'title' : fake.text(100),
            'description' : fake.text(500),
            'user_id' : 1,
        }),
        headers=headers,
        name = "Create a new recipe"
    )

def read(l):
    fake = Faker('es_ES')
    rand_entity = 'recipes'
    rand_id = random.randint(1,189306)
    headers = {'content-type': 'application/json'}
    l.client.get("/" + rand_entity + "/" + str(rand_id) + "/read", headers=headers)

def update(l):
    fake = Faker('es_ES')
    rand_entity = 'recipes'
    rand_id = random.randint(1,189306)
    headers = {'content-type': 'application/json'}
    l.client.put("/" + rand_entity + "/" + str(rand_id) + "/update",
        data=json.dumps({
            'title' : 'Updated',
            'description' : fake.text(100),
            'user_id' : 1,
        }),
        headers=headers,
        name = "Update a new recipe"
    )

def delete(l):
    fake = Faker('es_ES')
    rand_entity = 'recipes'
    rand_id = random.randint(0,189306)
    headers = {'content-type': 'application/json'}
    l.client.delete("/" + rand_entity + "/"+ str(rand_id) + "/delete", headers=headers)




class UserTasks(TaskSet):
    tasks = {
        index_random: 300,
        index: 500,
        create: 400,
        update: random.randint(200,600),
    }


class WebsiteUser(HttpUser):
    tasks = [UserTasks]
    wait_time = between(10.0, 20.0)
