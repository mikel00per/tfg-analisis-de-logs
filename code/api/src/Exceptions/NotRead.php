<?php


namespace Api\Exceptions;


use \Exception;

class NotRead extends Exception
{
    protected $message = "Not read ";
}