<?php


namespace Api\Exceptions;


use \Exception;

class  NotUpdated extends Exception
{
    protected $message = 'Not updated ';
}