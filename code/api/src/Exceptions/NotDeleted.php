<?php


namespace Api\Exceptions;


use Exception;

class NotDeleted extends Exception
{
    protected $message  = "Not deleted ";
}