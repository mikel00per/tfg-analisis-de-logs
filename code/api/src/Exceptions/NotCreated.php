<?php


namespace Api\Exceptions;

use Exception;

class NotCreated extends Exception
{
    protected $message = "Not created ";
}