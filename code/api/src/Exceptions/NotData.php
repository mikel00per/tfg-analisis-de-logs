<?php

namespace Api\Exceptions;

use \Exception;

class NotData extends Exception
{
    protected $message = "Not data ";
}