<?php

namespace Api\Domain;

class Recipe extends ObjectDomain
{
    private $title;
    private $description;
    private $ingredients;
    private $steps;
    private $userId;

    public function __construct(array $params)
    {
        $this->title = $params['title'] ?? null;
        $this->description = $params['description'] ?? null;
        $this->ingredients = $params['ingredients'] ?? null;
        $this->steps = $params['steps'] ?? null;
        $this->userId = $params['user_id'] ?? null;

        parent::__construct($params);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return explode(PHP_EOL, $this->ingredients);
    }

    /**
     * @return array
     */
    public function getSteps(): array
    {
        return explode(PHP_EOL, $this->steps);
    }

    /**
     * @return string
     */
    public function getIngredientsToString(): string
    {
        return $this->ingredients;
    }

    /**
     * @return string
     */
    public function getStepsToString(): string
    {
        return $this->steps;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'ingredients' => explode(PHP_EOL.PHP_EOL, $this->ingredients),
            'steps' => explode(PHP_EOL.PHP_EOL, $this->steps),
            'user_id' => $this->userId
        ];
    }
}