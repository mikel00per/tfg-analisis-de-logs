<?php


namespace Api\Domain;


class RecipeCategory extends ObjectDomain
{
    private $recipeId;
    private $categoryId;

    public function __construct(array $params)
    {
        $this->recipeId = $params['recipe_id'] ?? null;
        $this->categoryId = $params['category_id'] ?? null;

        parent::__construct($params);
    }

    /**
     * @return string
     */
    public function getRecipeId(): string
    {
        return $this->recipeId;
    }

    /**
     * @param string $recipeId
     */
    public function setRecipeId(string $recipeId): void
    {
        $this->recipeId = $recipeId;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'recipe_id' => $this->recipeId,
            'category_id' => $this->categoryId
        ];
    }


}