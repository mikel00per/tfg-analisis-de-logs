<?php

namespace Api\Domain;

use JsonSerializable;

abstract class ObjectDomain implements JsonSerializable
{
    protected $id;

    public function __construct(array $params = null)
    {
        $this->setId($params['id']);
    }

    public function setId(int $id = null): void
    {
        if ($id === null) {
            $this->id = null;
        } else {
            $this->id = $id;
        }
    }

    public function getId(): string {
        return $this->id;
    }
}