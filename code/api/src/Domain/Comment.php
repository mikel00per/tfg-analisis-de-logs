<?php


namespace Api\Domain;

use JsonSerializable;

class Comment extends ObjectDomain
{
    private $userId;
    private $recipeId;
    private $comment;
    private $date;

    public function __construct($params)
    {
        $this->userId = $params['user_id']  ;
        $this->recipeId = $params['recipe_id'];
        $this->comment = $params['comment'];
        $this->date = $params['date'];
        parent::__construct($params);
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getRecipeId(): string
    {
        return $this->recipeId;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'recipe_id' => $this->recipeId,
            'comment' => $this->comment,
            'date' => $this->date,
        ];
    }

}