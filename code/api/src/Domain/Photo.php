<?php

namespace Api\Domain;

class Photo extends ObjectDomain
{
    private $recipeId;
    private $position;
    private $path;

    public function __construct(array $params)
    {
        $this->recipeId = $params['recipe_id'];
        $this->position = $params['position'];
        $this->path = $params['path'];
        parent::__construct($params);
    }

    /**
     * @return string
     */
    public function getRecipeId(): string
    {
        return $this->recipeId;
    }

    /**
     * @param string $recipeId
     */
    public function setRecipeId(string $recipeId): void
    {
        $this->recipeId = $recipeId;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'recipe_id' => $this->recipeId,
            'position' => $this->position,
            'path' => $this->path
        ];
    }
}