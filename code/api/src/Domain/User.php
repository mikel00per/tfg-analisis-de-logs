<?php

namespace Api\Domain;

class User extends ObjectDomain
{
    private $first_name;
    private $second_name;
    private $email;
    private $password;
    private $address;
    private $telephone;
    private $role_id;
    private $active;
    private $photo;
    private $activateToken;

    public function __construct(array $params)
    {
        $this->first_name = $params['first_name'] ?? null;
        $this->second_name = $params['second_name'] ?? null;
        $this->email = $params['email'] ?? null;
        $this->password = $params['password'] ?? null;
        $this->address = $params['address'] ?? null;
        $this->telephone = $params['telephone'] ?? null;
        $this->role_id = $params['role_id'] ?? null;
        $this->active = $params['active'] ?? null;
        $this->photo = $params['photo'] ?? null;
        $this->activateToken = $params['activateToken'] ?? null;

        parent::__construct($params);
    }

    /**
     * @return mixed
     */
    public function getFirstName() :string
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getSecondName(): string
    {
        return $this->second_name;
    }

    /**
     * @param mixed $second_name
     */
    public function setSecondName($second_name): void
    {
        $this->second_name = $second_name;
    }

    /**
     * @return mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getRole(): int
    {
        return $this->role_id;
    }

    /**
     * @param mixed $role_id
     */
    public function setRole($role_id): void
    {
        $this->role_id = $role_id;
    }

    /**
     * @return mixed
     */
    public function getActive() : bool
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getPhoto() :string
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getActivateToken()
    {
        return $this->activateToken;
    }

    /**
     * @param mixed $activateToken
     */
    public function setActivateToken($activateToken): void
    {
        $this->activateToken = $activateToken;
    }

    public function getRoleName(): string
    {
        if ($this->role_id == 1){
            return 'Administrador';
        }else{
            return 'Colaborador';
        }
    }

    public function getActiveName(): string
    {
        if ($this->active == 1){
            return 'Activado';
        }else{
            return 'Desactivador';
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'email' => $this->email,
            'password' => $this->password,
            'address' => $this->address,
            'telephone' => $this->telephone,
            'role_id' => $this->role_id,
            'active' => $this->active,
            'photo' => $this->photo,
            'activate_token' => $this->activateToken
        ];
    }
}