<?php


namespace Api\Domain;


use JsonSerializable;

class Role extends ObjectDomain
{
    private $role;

    public function __construct($params)
    {
        $this->role = $params['role'];

        parent::__construct($params);
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'role' => $this->role
        ];
    }
}