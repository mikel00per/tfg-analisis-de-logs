<?php

namespace Api\Domain;

class Category extends ObjectDomain
{
    /** @var string $name */
    private $name;

    /**
     * Category constructor.
     * @param $params array
     */
    public function __construct(array $params)
    {
        $this->name = $params['name'];
        parent::__construct($params);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}