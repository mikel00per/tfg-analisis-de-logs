<?php

namespace Api\Domain;

class Rating extends ObjectDomain
{
    private $userId;
    private $recipeId;
    private $rating;

    public function __construct($params)
    {
        $this->userId = $params['user_id'];
        $this->recipeId = $params['recipe_id'];
        $this->rating = $params['rating'];
        parent::__construct($params);
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getRecipeId()
    {
        return $this->recipeId;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }


    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id ,
            'user_id' => $this->userId ,
            'recipe_id' => $this->recipeId ,
            'rating' => $this->rating ,
        ];
    }
}