<?php

namespace Api\Controllers;

use Api\Repository\RecipeRepository;
use Elastic\Apm\ElasticApm;
use JsonException;

class HomeController extends AbstractController
{
    protected $repository = RecipeRepository::class;

    public function index(): void
    {
        //$transaction = $this->agent->startTransaction('Failing-Transaction');

        $transaction = ElasticApm::beginCurrentTransaction('rand', self::class);

        try {
            $repository = new $this->repository;
            $randRecipe = $repository->getRand();
        }catch (\Exception $e) {
            //$this->log->info('Rand collection ' . $_SERVER['REMOTE_ADDR']);
            $randRecipe = [];
        } finally {
            $transaction->end();
            $this->response->ok([
                'rand_recipe' => $randRecipe
            ]);
        }
    }

    /**
     * @throws JsonException
     */
    public function status(): void
    {
        $this->response->accept();
    }
}