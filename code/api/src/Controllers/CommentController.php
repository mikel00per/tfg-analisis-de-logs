<?php

namespace Api\Controllers;

use Api\Repository\CommentRepository;

class CommentController extends AbstractController
{
    protected $repository = CommentRepository::class;
    protected string $resource = 'comment';
    protected string $collection = 'comments';
}