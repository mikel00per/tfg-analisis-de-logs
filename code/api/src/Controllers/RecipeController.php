<?php

namespace Api\Controllers;

use Api\Repository\RecipeRepository;

class RecipeController extends AbstractController
{
    protected $repository = RecipeRepository::class;
    protected string $resource = 'recipe';
    protected string $collection = 'recipes';
}