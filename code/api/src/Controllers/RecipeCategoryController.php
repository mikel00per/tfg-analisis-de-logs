<?php

namespace Api\Controllers;

use Api\Repository\RecipeCategoryRepository;

class RecipeCategoryController extends AbstractController
{
    protected $repository = RecipeCategoryRepository::class;
    protected string $resource = 'recipe_category';
    protected string $collection = 'recipes_categories';
}