<?php

namespace Api\Controllers;

use Api\Repository\UserRepository;

class UserController extends AbstractController
{
    protected $repository = UserRepository::class;
    protected string $resource = 'user';
    protected string $collection = 'users';
}