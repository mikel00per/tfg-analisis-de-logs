<?php

namespace Api\Controllers;

use Api\Repository\CategoryRepository;

class CategoryController extends AbstractController
{
    protected $repository = CategoryRepository::class;
    protected string $resource = 'category';
    protected string $collection = 'categories';
}