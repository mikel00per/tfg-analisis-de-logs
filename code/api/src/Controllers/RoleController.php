<?php

namespace Api\Controllers;

use Api\Repository\RoleRepository;

class RoleController extends AbstractController
{
    protected $repository = RoleRepository::class;
    protected string $resource = 'role';
    protected string $collection = 'roles';
}