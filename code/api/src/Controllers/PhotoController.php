<?php

namespace Api\Controllers;

use Api\Repository\PhotoRepository;

class PhotoController extends AbstractController
{
    protected $repository = PhotoRepository::class;
    protected string $resource = 'photo';
    protected string $collection = 'photos';
}