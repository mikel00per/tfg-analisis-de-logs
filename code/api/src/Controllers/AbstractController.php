<?php

namespace Api\Controllers;

use Api\Exceptions\NotCreated;
use Api\Exceptions\NotData;
use Api\Exceptions\NotDeleted;
use Api\Exceptions\NotRead;
use Api\Exceptions\NotUpdated;
use Elastic\Apm\ElasticApm;
use Elasticsearch\ClientBuilder;
use JsonException;
use Logger\Request;
use Logger\Response;
use Monolog\Handler\ElasticsearchHandler;
use Monolog\Logger;
use PhilKra\Agent;
use PhilKra\Exception\Transaction\DuplicateTransactionNameException;
use PhilKra\Exception\Transaction\UnknownTransactionException;

abstract class AbstractController
{
    protected $repository;
    protected Response $response;
    protected Request $request;
    protected Logger $log;

    protected string $resource;
    protected string $collection;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->repository = new $this->repository();

        $client = ClientBuilder::create()
            ->setElasticCloudId('tfg-analisis-logs:dXMtZWFzdC0xLmF3cy5mb3VuZC5pbzo0NDMkNjI2YmE4YTdkMDM2NDg3YjhhNGViNDRhODJmMDc4YzIkOWI2YTgyZTk5MDc4NDljMzk1ZWNmOGU5ODczZjBkNDc=')
            ->setBasicAuthentication('elastic','ltFVNnCIiWdTbdgQGtu8vp6I')
            ->build();

        $handler = new ElasticsearchHandler($client, [
            'index' => 'monolog-local-'.date('Y-m'),
            'ignore_error' => true,
        ]);

        $this->log = new Logger('Logger');
        $this->log->pushHandler($handler);
    }

    /**
     *
     * @throws JsonException
     */
    public function index(): void
    {
        $transaction = ElasticApm::beginCurrentTransaction('index', get_class($this));

        if ($this->request->isGet()){

            try {
                $objectsDomain = $this->repository->index(1,1000);
                $this->log->info('Read collection '.$this->collection.' consumida. ', [ 'page' => 1,'length' => 20]);
                $this->response->accept([$this->collection => $objectsDomain]);
            } catch(NotData $e) {
                $this->response->noContent();
                $this->log->alert($e->getMessage() . $this->collection);
            }
        }else{
            $this->response->methodNotAllowed([]);
        }

        $transaction->end();
    }

    /**
     * @throws JsonException
     */
    public function create(): void
    {
        $transaction = ElasticApm::beginCurrentTransaction('create', get_class($this));

        if ($this->request->isPost()){
            $params = $this->request->params();

            try {
                $objectDomain = $this->repository->create($params);
                $this->log->info('Created '.$this->resource, [$this->resource => $objectDomain->jsonSerialize()]);
                $this->response->created([$this->resource => $objectDomain]);
            } catch (NotCreated $e) {
                $this->log->emergency($e->getMessage(), $params->toArray());
                $this->response->notAcceptable([$this->resource => []]);
            }
        }else{
            $this->response->methodNotAllowed();
        }

        $transaction->end();
    }

    /**
     * @throws JsonException
     */
    public function read($id): void
    {
        $transaction = ElasticApm::beginCurrentTransaction('read', get_class($this));

        if ($this->request->isGet()){

            try {
                $objectDomain = $this->repository->read($id);
                //$this->log->info('Read '.$this->resource, ['id' => $id]);
                $this->response->accept([$this->resource => $objectDomain]);
            } catch (NotRead $e) {
                $this->log->info($e->getMessage() . $this->resource, ['id' => $id]);
                $this->response->notFound([$this->resource => []]);
            }

        }else{
            $this->response->methodNotAllowed();
        }

        $transaction->end();
    }

    /**
     * @throws JsonException
     */
    public function update($id): void
    {
        $transaction = ElasticApm::beginCurrentTransaction('update', get_class($this));

        if ($this->request->isPut()){
            $params = $this->request->params();

            try {
                $objectDomain = $this->repository->update($id, $params);
                $this->log->info('Updated '.$this->resource, ['id' => $id, 'params' => $objectDomain->toArray()]);
                $this->response->accept([$this->resource => $objectDomain]);
            } catch (NotUpdated $e) {
                $this->log->alert($e->getMessage().$this->resource, ['id' => $id, 'params' => $params->toArray()]);
                $this->response->notModified([$this->resource => []]);
            }

        }else{
            $this->response->methodNotAllowed();
        }

        $transaction->end();
    }

    /**
     * @throws JsonException
     */
    public function delete($id): void
    {
        $transaction = ElasticApm::beginCurrentTransaction('delete', get_class($this));

        if ($this->request->isDelete()){
            $model = new $this->repository();

            try {
                $model->delete($id);
                $this->log->info('Deleted ' . $this->resource, ['id' => $id]);
                $this->response->noContent();
            } catch (NotDeleted $e) {
                $this->log->debug($e->getMessage() . $this->resource);
                $this->response->notFound([$this->resource => []]);
            }

        }else{
            $this->response->methodNotAllowed();
        }

        $transaction->end();
    }

    /**
     * @throws JsonException
     */
    public function search(): void
    {
        if ($this->request->isPost()){
            $this->response->notImplemented();
        }else{
            $this->response->methodNotAllowed();
        }
    }

    /**
     * @throws JsonException
     */
    protected function render(string $template, array $data): void
    {
        $this->response->notAcceptable( $data );
    }
}