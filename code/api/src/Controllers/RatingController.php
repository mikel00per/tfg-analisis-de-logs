<?php

namespace Api\Controllers;

use Api\Repository\RatingRepository;

class RatingController extends AbstractController
{
    protected $repository = RatingRepository::class;
    protected string $resource = 'rating';
    protected string $collection = 'ratings';
}