<?php

namespace Api\Repository;

use Api\Domain\Category;
use Api\Repository\DatabaseRepository;

use PDO;

class CategoryRepository extends DatabaseRepository
{
    protected $domain = Category::class;

    protected $table = 'categories';

    protected $primary_key = 'id';

    protected $foreign_keys = [

    ];

    protected $fill = [
        'name'
    ];

    protected $cast = [
        'name' => PDO::PARAM_STR
    ];

}
