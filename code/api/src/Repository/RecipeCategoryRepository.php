<?php

namespace Api\Repository;

use PDO;

class RecipeCategoryRepository extends DatabaseRepository
{
    protected $domain = '\Api\Domain\RecipeCategory';

    protected $table = 'recipes_categories';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'recipe_id',
        'category_id'
    ];

    protected $fill = [
        'recipe_id',
        'category_id',
    ];

    protected $cast = [
        'recipe_id' => PDO::PARAM_INT,
        'category_id' => PDO::PARAM_INT,
    ];
}