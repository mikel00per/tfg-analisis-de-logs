<?php

namespace Api\Repository;

use Api\Domain\Comment;
use PDO;

class CommentRepository extends DatabaseRepository
{
    protected $domain = Comment::class;

    protected $table = 'comments';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'user_id',
        'recipe_id'
    ];

    protected $fill = [
        'user_id',
        'recipe_id',
        'comment',
        'date'
    ];

    protected $cast = [
        'user_id' => PDO::PARAM_INT,
        'recipe_id' => PDO::PARAM_INT,
        'comment' => PDO::PARAM_STR,
        'date' =>  PDO::PARAM_STR
    ];
}