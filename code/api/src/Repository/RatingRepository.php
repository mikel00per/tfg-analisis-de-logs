<?php


namespace Api\Repository;

use PDO;

class RatingRepository extends DatabaseRepository
{
    protected $domain = '\Api\Domain\Rating';

    protected $table = 'ratings';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'user_id',
        'recipe_id',
    ];

    protected $fill = [
        'user_id',
        'recipe_id',
        'rating'
    ];

    protected $cast = [
        'user_id' => PDO::PARAM_INT,
        'recipe_id' => PDO::PARAM_INT,
        'rating' => PDO::PARAM_INT,
    ];
}