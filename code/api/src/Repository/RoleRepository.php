<?php

namespace Api\Repository;

use PDO;

class RoleRepository extends DatabaseRepository
{
    protected $domain = '\Api\Domain\Role';

    protected $table = 'roles';

    protected $primary_key = 'id';

    protected $foreign_keys = [

    ];

    protected $fill = [
        'role',
    ];

    protected $cast = [
        'role' => PDO::PARAM_STR,
    ];
}