<?php

namespace Api\Repository;

use Api\Domain\ObjectDomain;
use Api\Exceptions\NotCreated;
use Api\Exceptions\NotData;
use Api\Exceptions\NotDeleted;
use Api\Exceptions\NotRead;
use Api\Exceptions\NotUpdated;
use Exception;
use Logger\DataBase;
use Logger\Parameters;
use PDO;

class DatabaseRepository implements RepositoryInterface
{
    /** @var PDO $db */
    public $db;

    /** @var string $domain */
    protected $domain;

    /** @var string $table */
    protected $table;

    /** @var string $primary_key */
    protected $primary_key = 'id';

    /** @var array $fill */
    protected $fill;

    /** @var array $cast */
    protected $cast;

    /**
     *
     */
    public function __construct()
    {
        $this->db = DataBase::getInstance();
    }

    /**
     *
     * @param int $page
     * @param int $pageLength
     *
     * @return array
     *
     * @throws NotData
     */
    public function index(int $page = 1, int $pageLength = 1000): array
    {
        $this->checkDb();

        $start = $pageLength * ($page - 1);
        $query = 'SELECT * FROM ' . $this->table . ' LIMIT :page, :length';

        $sth = $this->db->prepare($query);
        $sth->bindParam('page', $start, PDO::PARAM_INT);
        $sth->bindParam('length', $pageLength, PDO::PARAM_INT);
        $sth->execute();

        $rows = $sth->fetchAll();
        $objs = [];


        foreach ($rows as $row) {
            $objs[] = new $this->domain($row);
        }

        if (count($objs) === 0) {
            throw new NotData();
        }


        return $objs;
    }

    /**
     * @param Parameters $data
     * @return ObjectDomain
     *
     * @throws NotCreated
     */
    public function create(Parameters $data): ObjectDomain
    {
        $this->checkDb();
        $objectDomain = new $this->domain($data->toArray());

        $query = "INSERT INTO " . $this->table . " (" . implode(",", $this->fill) . ") VALUES (";

        foreach ($objectDomain->jsonSerialize() as $key => $value) {
            if ($key === 'id') {
                continue;
            }

            $query .= ":$key";
            if (end($this->fill) !== $key) {
                $query .= ', ';
            }
        }
        $query .= ")";

        $sth = $this->db->prepare($query);

        foreach ($objectDomain->jsonSerialize() as $key => $value) {
            if ($key === 'id') {
                continue;
            }

            $sth->bindValue(':' . $key, $value, $this->cast[$key]);
        }

        $result = $sth->execute();

        if (!($result)) {
            throw new NotCreated();
        }

        $id = $this->db->lastInsertId();
        $objectDomain->setId($id);

        return $objectDomain;
    }

    /**
     * @throws NotRead
     */
    public function read(int $id): ObjectDomain
    {
        $this->checkDb();

        $query = "SELECT * FROM " . $this->table . " WHERE id=:id";

        $sth = $this->db->prepare($query);

        $sth->bindValue(':id', $id, PDO::PARAM_INT);
        $sth->execute();
        $row = $sth->fetch();

        if (empty($row)) {
            throw new NotRead();
        }

        return new $this->domain($row);
    }

    /**
     * @throws NotUpdated
     * @throws NotRead
     */
    public function update(int $id, Parameters $data): ObjectDomain
    {
        $this->checkDb();
//
//        try {
//            $objectDomainPersisted = $this->read($id);
//        } catch (NotRead $e) {
//            throw new NotUpdated('El source no existe');
//        }

        $objectDomain = new $this->domain(array_merge(['id' => $id], $data->toArray()));

        $query = "UPDATE " . $this->table . " SET ";

        foreach ($this->fill as $colName) {
            $query .= "$colName=:$colName";
            if (end($this->fill) !== $colName) {
                $query .= ', ';
            }
        }

        $query .= " WHERE id=:id";

        $sth = $this->db->prepare($query);

        $sth->bindValue(':id', $id, PDO::PARAM_INT);

        foreach ($objectDomain->jsonSerialize() as $key => $value) {
            if ($key === 'id') {
                continue;
            }

            $sth->bindValue(':' . $key, $value, $this->cast[$key]);
        }

        $result = $sth->execute();

        if (!$result) {
            throw new NotUpdated();
        }

        return $objectDomain;
    }

    /**
     * @param int $id
     *
     * @return bool
     *
     * @throws NotDeleted
     */
    public function delete(int $id): bool
    {
        $this->checkDb();

        $query = "DELETE FROM " . $this->table . " WHERE id=:id";
        $sth = $this->db->prepare($query);

        $sth->bindValue(':id', $id, PDO::PARAM_INT);
        $sth->execute();

        $result = $sth->rowCount();
        if ($result === 0) {
            throw new NotDeleted();
        }

        return true;
    }

    /**
     * @param array $filters
     */
    public function search(array $filters)
    {
        // TODO: Implement search() method.
    }

    public function last(): ObjectDomain
    {
        $this->checkDb();

        $query = "SELECT * FROM " . $this->table . " ORDER BY ID DESC LIMIT 1";

        $sth = $this->db->query($query);
        $row = $sth->fetch();

        if ($row) {
            return new $this->domain($row);
        }

        return new $this->domain([]);
    }

    /**
     * @throws Exception
     */
    public function getRand(): ObjectDomain
    {
        $this->checkDb();

        $query = "SELECT * FROM " . $this->table . " ORDER BY RAND() LIMIT 1";
        $sth = $this->db->query($query);
        $row = $sth->fetch();

       // throw new Exception("ERROR :D");

        if ($row) {
            return new $this->domain($row);
        }

        return new $this->domain([]);
    }

    private function checkDb(): void
    {
        if ($this->db === null) {
            $this->db = DataBase::getInstance();
        }
    }
}