<?php

namespace Api\Repository;

use Api\Domain\User;
use PDO;

class UserRepository extends DatabaseRepository
{
    protected $domain = User::class;

    protected $table = 'users';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'role_id'
    ];

    protected $fill = [
        'first_name',
        'second_name',
        'email',
        'password',
        'address',
        'telephone',
        'role_id',
        'active',
        'photo',
        'activate_token'
    ];

    protected $cast = [
        'first_name' => PDO::PARAM_STR,
        'second_name' => PDO::PARAM_STR,
        'email' => PDO::PARAM_STR,
        'password' => PDO::PARAM_STR,
        'address' => PDO::PARAM_STR,
        'telephone' => PDO::PARAM_STR,
        'role_id' => PDO::PARAM_INT,
        'active' => PDO::PARAM_BOOL,
        'photo' => PDO::PARAM_STR,
        'activate_token' => PDO::PARAM_STR
    ];

}