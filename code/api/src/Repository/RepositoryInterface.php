<?php

namespace Api\Repository;

use Api\Domain\ObjectDomain;
use Logger\Parameters;

interface RepositoryInterface
{
    public function index(int $page, int $pageLength);
    public function create(Parameters $data): ObjectDomain;
    public function read(int $id): ObjectDomain;
    public function update(int $id, Parameters $data);
    public function delete(int $id);
    public function search(array $filters);
}