<?php


namespace Api\Repository;

use Api\Domain\Recipe;
use PDO;


class RecipeRepository extends DatabaseRepository
{
    protected $domain = Recipe::class;

    protected $table = 'recipes';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'user_id',
    ];

    protected $fill = [
        'title',
        'description',
        'ingredients',
        'steps',
        'user_id',
    ];

    protected $cast = [
        'title' => PDO::PARAM_STR,
        'description' => PDO::PARAM_STR,
        'ingredients' => PDO::PARAM_STR,
        'steps' => PDO::PARAM_STR,
        'user_id' => PDO::PARAM_INT,
    ];


}
