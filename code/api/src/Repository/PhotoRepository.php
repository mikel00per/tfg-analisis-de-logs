<?php


namespace Api\Repository;

use Api\Domain\Photo;
use PDO;

class PhotoRepository extends DatabaseRepository
{
    protected $domain = Photo::class;

    protected $table = 'photos';

    protected $primary_key = 'id';

    protected $foreign_keys = [
        'recipe_id',
    ];

    protected $fill = [
        'recipe_id',
        'position',
        'path',
    ];

    protected $cast = [
        'recipe_id' => PDO::PARAM_INT,
        'position' => PDO::PARAM_INT,
        'path' => PDO::PARAM_STR,
    ];

}
