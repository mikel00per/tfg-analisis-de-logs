DROP TABLE categories;

CREATE TABLE `categories` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO categories VALUES(1,"Carnes");
INSERT INTO categories VALUES(2,"Pescados");
INSERT INTO categories VALUES(3,"Arroz");
INSERT INTO categories VALUES(4,"Sopa");
INSERT INTO categories VALUES(5,"Fácil");
INSERT INTO categories VALUES(6,"Difícil");
INSERT INTO categories VALUES(7,"Ligero");
INSERT INTO categories VALUES(8,"Pesado");



DROP TABLE comments;

CREATE TABLE `comments` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) DEFAULT NULL,
    `recipe_id` int(11) DEFAULT NULL,
    `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `users_comments_fk` (`user_id`),
    KEY `users_comments_recipes_fk` (`recipe_id`),
    CONSTRAINT `users_comments_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
    CONSTRAINT `users_comments_recipes_fk` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO comments VALUES(5,1,36,"La he probado y ha quedado super bien. Muchas gracias!!","2020-06-13 23:15:23");
INSERT INTO comments VALUES(6,1,35,"ME ENCANTA!!","2020-06-13 23:15:52");
INSERT INTO comments VALUES(7,1,37,"Enamorado de ese tipo de empanadas!!","2020-06-13 23:16:28");
INSERT INTO comments VALUES(8,3,36,"Parece super esponjoso, que ricoo","2020-06-13 23:17:04");
INSERT INTO comments VALUES(9,3,39,"No se ve la imagen!","2020-06-13 23:17:30");
INSERT INTO comments VALUES(10,3,35,"Menuda pintaza :)","2020-06-13 23:17:52");
INSERT INTO comments VALUES(11,30,34,"DONUTSSS!!","2020-06-13 23:19:03");
INSERT INTO comments VALUES(13,2,32,"Hola","2020-06-13 23:34:34");
INSERT INTO comments VALUES(14,2,36,"Que bonito ha quedado!","2020-06-13 23:35:16");



DROP TABLE log_activity;

CREATE TABLE `log_activity` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `date` datetime DEFAULT NULL,
    `description` text COLLATE utf8mb4_unicode_ci,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5340 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE log_error;

CREATE TABLE `log_error` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `date` datetime DEFAULT NULL,
     `description` text COLLATE utf8mb4_unicode_ci,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1075 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE photos;

CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `photos_recipes__fk` (`recipe_id`),
  CONSTRAINT `photos_recipes__fk` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO photos VALUES(28,32,1,"images/5ee532fb3b943.jpg");
INSERT INTO photos VALUES(29,32,2,"images/5ee5330f81575.jpg");
INSERT INTO photos VALUES(30,32,3,"images/5ee5331ec624c.jpg");
INSERT INTO photos VALUES(31,32,4,"images/5ee53363ec747.jpg");
INSERT INTO photos VALUES(32,32,5,"images/5ee533cec74df.jpg");
INSERT INTO photos VALUES(33,32,6,"images/5ee533db3e56f.jpg");
INSERT INTO photos VALUES(34,33,1,"images/5ee53581f12c9.jpg");
INSERT INTO photos VALUES(35,33,2,"images/5ee5358992258.jpg");
INSERT INTO photos VALUES(36,33,3,"images/5ee535914275d.jpg");
INSERT INTO photos VALUES(37,33,4,"images/5ee5359c427fd.jpg");
INSERT INTO photos VALUES(38,33,5,"images/5ee535a2aed46.jpg");
INSERT INTO photos VALUES(39,33,6,"images/5ee535a8aff12.jpg");
INSERT INTO photos VALUES(40,33,7,"images/5ee535bad73a0.jpg");
INSERT INTO photos VALUES(41,33,8,"images/5ee535c2afb9e.jpg");
INSERT INTO photos VALUES(42,33,9,"images/5ee535c961cde.jpg");
INSERT INTO photos VALUES(43,34,1,"images/5ee536ac5adbd.jpg");
INSERT INTO photos VALUES(44,34,2,"images/5ee536b2c48de.jpg");
INSERT INTO photos VALUES(45,34,3,"images/5ee536b7b273f.jpg");
INSERT INTO photos VALUES(46,34,4,"images/5ee536bce2a7c.jpg");
INSERT INTO photos VALUES(47,34,5,"images/5ee536c66b903.jpg");
INSERT INTO photos VALUES(48,34,6,"images/5ee536cd8928f.jpg");
INSERT INTO photos VALUES(49,35,1,"images/5ee53936c4ba6.jpg");
INSERT INTO photos VALUES(50,35,2,"images/5ee5393c0f46c.jpg");
INSERT INTO photos VALUES(51,35,3,"images/5ee53946010f2.jpg");
INSERT INTO photos VALUES(52,35,4,"images/5ee5394ac4bd9.jpg");
INSERT INTO photos VALUES(53,35,5,"images/5ee539518cc2a.jpg");
INSERT INTO photos VALUES(54,35,6,"images/5ee53989d6266.jpg");
INSERT INTO photos VALUES(55,36,1,"images/5ee53a8822085.jpg");
INSERT INTO photos VALUES(56,36,2,"images/5ee53a8c809a7.jpg");
INSERT INTO photos VALUES(57,36,3,"images/5ee53a98abefe.jpg");
INSERT INTO photos VALUES(58,36,4,"images/5ee53aa3ac8e1.jpg");
INSERT INTO photos VALUES(59,36,5,"images/5ee53aab35a9e.jpg");
INSERT INTO photos VALUES(60,36,6,"images/5ee53ab323d22.jpg");
INSERT INTO photos VALUES(61,36,7,"images/5ee53abe44504.jpg");
INSERT INTO photos VALUES(62,36,8,"images/5ee53ac640cad.jpg");
INSERT INTO photos VALUES(63,36,9,"images/5ee53acd60f00.jpg");
INSERT INTO photos VALUES(64,36,10,"images/5ee53ad3dd08d.jpg");
INSERT INTO photos VALUES(68,37,1,"images/5ee53c094130a.jpg");
INSERT INTO photos VALUES(69,37,2,"images/5ee53c13ac2b8.jpg");
INSERT INTO photos VALUES(70,37,3,"images/5ee53c181978f.jpg");
INSERT INTO photos VALUES(71,37,4,"images/5ee53c1cb9434.jpg");
INSERT INTO photos VALUES(72,37,5,"images/5ee53c20eda4a.jpg");
INSERT INTO photos VALUES(73,37,6,"images/5ee53c26e0f7d.jpg");
INSERT INTO photos VALUES(74,37,7,"images/5ee53c2c511d4.jpg");
INSERT INTO photos VALUES(75,37,8,"images/5ee53c3407e09.jpg");
INSERT INTO photos VALUES(76,37,9,"images/5ee53c40140bc.jpg");
INSERT INTO photos VALUES(77,37,10,"images/5ee53c4544d6b.jpg");
INSERT INTO photos VALUES(78,38,1,"images/5ee53d7a5ae7b.jpg");
INSERT INTO photos VALUES(79,38,2,"images/5ee53d8146015.jpg");
INSERT INTO photos VALUES(80,38,3,"images/5ee53d864fbba.jpg");
INSERT INTO photos VALUES(81,38,4,"images/5ee53d8c6e029.jpg");
INSERT INTO photos VALUES(82,38,5,"images/5ee53d932a27c.jpg");
INSERT INTO photos VALUES(86,40,1,"images/5ee53f1b7311a.jpg");
INSERT INTO photos VALUES(87,40,2,"images/5ee53f2083e56.jpg");
INSERT INTO photos VALUES(88,40,3,"images/5ee53f268761a.jpg");
INSERT INTO photos VALUES(89,40,4,"images/5ee53f2b11fc5.jpg");
INSERT INTO photos VALUES(90,40,5,"images/5ee53f2f92189.jpg");
INSERT INTO photos VALUES(91,39,1,"images/5ee54d247894e.jpg");
INSERT INTO photos VALUES(92,39,2,"images/5ee54d2b7ce4f.jpg");
INSERT INTO photos VALUES(93,39,3,"images/5ee54d30382ff.jpg");
INSERT INTO photos VALUES(94,39,4,"images/5ee54d3580d97.jpg");



DROP TABLE ratings;

CREATE TABLE `ratings` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `user_id` int(11) DEFAULT NULL,
   `recipe_id` int(11) DEFAULT NULL,
   `rating` int(11) DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `recipes_fk` (`recipe_id`),
   KEY `users_fk` (`user_id`),
   CONSTRAINT `recipes_fk` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`),
   CONSTRAINT `users_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




DROP TABLE recipes;

CREATE TABLE `recipes` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
   `description` text COLLATE utf8mb4_unicode_ci,
   `ingredients` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
   `steps` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
   `user_id` int(11) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO recipes VALUES(32,"Receta de Masa para tacos","Hacer las tortillas de maíz para tacos en casa es muy fácil y el resultado nada tiene que envidiar a tortillas de supermercado. Además, la receta es muy sencilla y con muy pocos ingredientes. Una vez listas, podrás disfrutarlas tanto calientes como frías, y puedes rellenarlas en el momento o guardarlas en la nevera bien tapadas.
\n
\nEsta receta de masa para tacos te servirá para rellenarlos con los ingredientes que más te gusten: aguacate, piña, mango, rúcula, gambas, pollo o lo que tengas por la nevera de casa. Ya verás que son más fáciles de hacer de lo que parecen, así que toma buena nota de los ingredientes.","175 gramos de harina de maíz precocida
\n300 mililitros de agua tibia
\n1 pizca de sal","Pon en un bol la harina con una pizca de sal. Ve añadiendo poco a poco el agua tibia mientras vas removiendo con una cuchara hasta conseguir una masa pegajosa pero manejable. Quizás no necesites añadir toda el agua, dependerá de la que admita tu harina.
\nUna vez tengas la masa para tacos casera, coge una pequeña porción del tamaño de una ciruela o algo mayor. Lo ideal es tener una prensa o tortillera, poner en ella la masa y simplemente prensar para sacar la tortita. No obstante, si no tienes, puedes poner la porción entre dos láminas de papel de horno o de papel film y estirar la masa con un rodillo hasta dejarla finita. Nosotros preferimos usar papel de horno.
\nRetira el papel de horno superior y corta la tortilla. Puedes utilizar un aro grande de emplatar o el aro de un molde desmontable, como hemos hecho nosotros. También puedes utilizar un plato hondo no muy grande o un bol.
\nCalienta una plancha, puedes usar una grande eléctrica o una convencional al fuego. Pon las tortitas encima, te puedes ayudar con el papel de horno para echarlas sin que se rompan o pierdan la forma. Deja hacer por las dos caras.
\nVe depositando las tortillas mexicanas unas encimas de las otras y tápalas con papel de aluminio si quieres conservarlas calientes, aunque son más fáciles de manejar cuando están frías.
\n¡Y ya están listas para rellenar de lo que prefieras! Si te sobra masa para tacos o tortillas, envuelve en papel film y guarda en la nevera hasta cuatro días.",1);
INSERT INTO recipes VALUES(33,"Receta de Albóndigas de bacalao","Si quieres aprender cómo hacer albóndigas de bacalao con una receta fácil y rápida, no te pierdas este paso a paso que te mostramos en RecetasGratis. Se trata de unas albóndigas de bacalao caseras que no llevan ningún tipo de harina, sino que se preparan con puré de patatas cocidas. Por ello, son aptas para celíacos, ya que no contienen gluten, e ideal para intolerantes a la lactosa, puesto que tampoco tiene leche.","100 gramos de bacalao desmenuzado desalado
\n2 patatas grandes
\n1 huevo
\n2 gramos de pimentón dulce (paprika)
\n3 gramos de perejil
\n1 diente de ajo
\n1 litro de aceite para freír
\n1 pizca de sal
\n1 pizca de pimienta","Deja listos todos los ingredientes para empezar con la elaboración de las albóndigas de bacalao caseras.
\nPela y cocina las patatas en agua hirviendo y sal durante aproximadamente 35 minutos, o hasta que se pueda introducir un cuchillo en ellas y se deslice con facilidad.
\nBate el huevo con el ajo machacado. De esta forma, tus albóndigas de bacalao tendrá muchísimo más sabor.
\nTritura las patatas hasta hacerlas puré y añade el pimentón dulce (paprika), el perejil finamente picado, el huevo batido, la sal y la pimienta.
\nAgrega el bacalao desmenuzado y mezcla muy bien. La masa de las albóndigas de bacalao debe quedar homogénea y con la consistencia adecuada como para trabajarla con las manos.
\nCoge pequeñas porciones de masa y haz las albóndigas de bacalao caseras dándoles la forma que más te guste, redondeada o más bien alargada. Nosotros hemos optado por hacer cilindros de 5 cm de largo y 3 cm de ancho, tipo croquetas.
\nFríe las albóndigas de bacalao y patata en abundante aceite a temperatura media hasta que estén bien doradas.
\nDéjalas sobre papel de cocina absorbente para retirar el exceso de aceite y listo. Ya puedes servir esta maravillosa receta de albóndigas de bacalao para disfrutarla con la salsa y los acompañamientos que más te gusten. Por ejemplo, puedes degustarlas con una salsa verde o acompañarlas con crema agria o alioli casero.
\nYa has aprendido cómo se hacen las albóndigas de bacalao, pero recuerda que puedes prepararlas de una forma más rápida y cómoda con el robot de cocina Mycook Touch de Taurus. Además, tenemos un código con 500 € de descuento: RECETASMYCOOK. ¡Aprovéchalo y consigue tu robot a un precio increíble! Solo estará disponible para los 15 primeros.",1);
INSERT INTO recipes VALUES(34,"Receta de Rosquillas caseras dulces","Aunque se comen en muchos países, las rosquillas son especialmente famosas en España, donde se preparan para las celebraciones de Semana Santa y Pascua. Estas rosquillas tradicionales reciben el nombre de rosquillas de Santa Clara y se preparan con anís. Pero en este caso, haremos una receta de rosquillas caseras más universal, perfecta para prepararla sin importar dónde estés y darle un toque dulce a tu semana. Podrás decorarlas como quieras y disfrutarlas cálidas y recién hechas.","2 huevos
\n80 mililitros de leche entera
\n80 mililitros de aceite neutro
\n5 gramos de levadura química en polvo (polvo para hornear
\n250 gramos de harina de trigo
\n150 gramos de azúcar (¾ taza)
\n400 mililitros de aceite para freír","Para empezar a hacer la masa de estas rosquillas caseras de la abuela, mezcla los huevos en un tazón con el aceite, la leche y el azúcar. Bate todo bien hasta obtener una consistencia homogénea.
\nTamiza la harina con el polvo de hornear (levadura química) y agrégalos a la mezcla para formar una masa. Recuerda que cernir significa pasar por un colador fino, esto se hace para evitar que se formen grumos en la masa de las rosquillas caseras sin anís.
\nAmasa por unos cuantos minutos hasta que la masa de las rosquillas caseras esponjosas no se peque a la superficie.
\nDivide la masa en bolas de tamaño mediano. Luego, estira formando un cilindro y une las puntas como se ve en la foto. La cantidad de rosquillas fritas dependerá del tamaño que las hagas.
\nFríe a temperatura media hasta que las rosquillas estén doradas. Recuerda dejar que el aceite se caliente bien y ve controlando la temperatura para que las rosquillas no se quemen antes de estar hechas. Si buscas una receta más saludable, puedes probar estas rosquillas caseras al horno.
\nUna vez salgan del aceite, pasa las rosquillas caseras dulces por papel absorbente y espolvorea con azúcar blanca. Y si quieres probar otras recetas parecidas puedes echarle un vistazo a estas rosquillas napolitanas o a estas rosquillas de naranja y anís.",1);
INSERT INTO recipes VALUES(35,"Receta de Pollo a la mostaza y miel al horno","Si eres de esas personas que cocina recetas con pollo cada semana es normal que te quedes sin ideas para variar un poco el menú. Con esto en mente, en RecetasGratis te sugerimos que pruebes esta receta de pollo a la mostaza y miel, un platotan jugoso y sabroso que sin duda satisfará a toda la familia."," ½ pollo de corral o gallina campera
\n 1 naranja (jugo)
\n 3 cucharadas soperas de mostaza
\n 2 cucharadas soperas de miel
\n 2 dientes de ajo
\n 2 ramas de romero
\n 1 pizca de pimienta negra
\n 1 pizca de sal","Corta el pollo en trozos (mejor en octavos que en cuartos para que así se cocine antes el pollo con mostaza y miel al horno). Nosotros he utilizado un pollo campero porque la carne es más sabrosa y consistente y a la vez tiene menos grasa, pero tú puedes utilizar otro tipo de pollo o gallina, e incluso hacer la receta solo con los muslos, las alitas o las pechugas.
\nColoca el pollo cortado en una fuente de horno (que no necesitas engrasar) y espolvorea las hojas de romero, un poco de sal y pimienta al gusto.
\nPara hacer la salsa que acompañará el pollo al horno, introduce en el vaso de la batidora o licuadora el zumo de naranja con la mostaza, la miel y los ajos pelados y tritúralo todo.
\nAñade la salsa de mostaza y miel al pollo y mezcla girando las piezas varias veces para que se empapen por completo. Lo ideal es dejar que el pollo se marine en esta mezcla durante 1-2 horas, pero, si no tienes tiempo, entonces precalienta el horno a 180 °C y cocina el pollo con la mostaza y miel hasta que se haga bien, durante unos 40-45 minutos aproximadamente. A media cocción, remoja la carne con la salsa para que no se seque y quede bien jugosa.
\nComprueba que el pollo al horno con mostaza y miel está en su punto y sírvelo recién hecho para disfrutar de este plato caliente. Como ves, se trata de una receta de pollo con miel y mostaza al horno fácil, rápida y muy sabrosa. Puedes acompañar este plato con un salteado de verduras y setas o con puré de patatas al limón, por ejemplo. ¡Estupenda combinación!",30);
INSERT INTO recipes VALUES(36,"Receta de Bizcocho de yogur natural","El bizcocho de yogur es uno de los más típicos y populares en todo el mundo, ya que es económico, fácil y delicioso. Por dentro queda esponjoso, jugoso y muy tierno, por lo que es habitual emplearlo como base de pasteles. Para hacer este bizcocho es preciso usar un yogur natural de unos 125 gramos, aunque si solo tienes yogures de sabores también puedes usarlo. El vaso del yogur es el que necesitarás para medir el resto de los ingredientes, así que vacía el contenido en un recipiente, limpia el vaso y utilízalo para medir primero el azúcar y la harina y, después, el aceite.","1 yogur natural
\n4 huevos
\n3 vasos de harina (usa el vaso del yogur)
\n2 vasos de azúcar
\n1 vaso de aceite de girasol
\n1 sobre de levadura química en polvo o polvos de hornear (15 g)
\n1 limón (opcional)
\n3 cucharadas soperas de azúcar glas","Precalienta el horno a 180 ºC con calor arriba y abajo. Unta un molde redondo de 22-24 cm de diámetro con un poco de mantequilla o aceite y forra la base con papel vegetal, así el bizcocho de yogur se desmoldará mejor.
\nRecuerda que debes usar el vaso del yogur para medir todos los ingredientes. Así pues, introduce el yogur en un recipiente y resérvalo. Luego, bate los huevos y el azúcar con varillas eléctricas. Si no tienes, puedes usar varillas manuales.
\nBate durante 2-3 minutos para que la mezcla doble su volumen. Debe quedar blanquecina y espumosa, ya que esta es una de las claves para conseguir un bizcocho de yogur esponjoso.
\nAñade el yogur natural a la mezcla anterior y sigue batiendo para integrarlo.
\nLava el limón, ralla la parte amarilla y agrega esta ralladura a la masa. Remueve y mezcla. Esto es totalmente opcional, ya que también puedes aromatizar tu receta de bizcocho de yogur con ralladura de naranja, esencia de vainilla, canela en polvo o dejarla tal cual.
\nTamiza la harina con la levadura y añade la mitad de esta mezcla a la masa. Intégrala por completo.
\nVierte el vaso de aceite de girasol, mezcla bien y añade la harina restante poco a poco y batiendo.
\nVierte la masa en el molde y hornea el bizcocho de yogur natural durante 30-40 minutos, o hasta que al pinchar el centro con un palillo este salga limpio. El tiempo puede variar según el horno, de manera que vigila el bizcocho a partir de los 30 minutos. Antes de este tiempo no debes abrir el horno o tu bizcocho se bajará. Te lo explicamos en este artículo: \\\\\\\"Por qué se baja el bizcocho cuando lo saco del horno\\\\\\\".
\nCuando esté el bizcocho listo, sácalo y deja que se enfríe un poco antes de desmoldarlo. Un truco para que se mantenga el bizcocho de yogur esponjoso es pagar el horno unos 5 minutos antes para que el cambio de temperatura no sea tan brusco.
\nUna vez frío, desmóldalo con cuidado y espolvorea azúcar glas por encima. ¡Lista tu receta de bizcocho de yogur natural! Como ves, se trata de un bizcocho de yogur fácil, esponjoso, rápido y muy jugoso. Anímate a experimentar con la decoración y prepara un pastel más completo.",30);
INSERT INTO recipes VALUES(37,"Receta de Empanadas de jurel","Esta receta de empanadas de jurel es ideal para disfrutar como plato principal tanto en el almuerzo como en la cena, ya que ofrece un aporte equilibrado de nutrientes, proteína de buena calidad rica en omega 3, grasas saludables e hidratos complejos.","150 gramos de jurel
\n1 cebolla
\n1 rama de perejil
\n1 huevo cocido (opcional)
\n4 masas para empanadas
\n400 mililitros de aceite para freír","En primer lugar, limpia el jurel, quitando las espinas y la piel del pescado para proceder a desmenuzarlo.
\nLava la cebolla y el perejil, corta la cebolla en cubitos pequeños y el perejil lo más pequeño posible.
\nEn una sartén con un chorrito de aceite, sofríe la cebolla hasta que se torne un poco transparente.
\nAgrega el perejil y el jurel al sofrito, sazona al gusto con sal y pimienta.
\nExtiende una masita para empanada y rellena con el jurel.
\nAgrega el huevo troceado si gustas y cierra los bordes de la empanada con ayuda de un tenedor.
\nCalienta el aceite por al menos 5 minutos. Con cuidado, sumerge una de las empanadas de jurel para que se fría por un lado.
\nPasados unos 3 a 5 minutos, voltea la empanadita para freír el otro lado, deben quedar doradas por ambos lados.
\nSirve las empanadas fritas de jurel mientras están calentitas y ¡a disfrutar!",3);
INSERT INTO recipes VALUES(38,"Receta de Ajoblanco malagueño","Conforme se acercan los días de calor y va haciendo mejor tiempo, las comidas cambian sus texturas y temperaturas, nos van apeteciendo otro tipo de alimentos más frescos y con ingredientes más sencillos, mientras prescindimos de los platos muy calientes de cuchara.","200 gramos de almendra cruda
\n2 dientes de ajo
\n1 rodaja de pan con mucha miga una rodaja gruesa
\n1 litro de agua fresca
\n30 mililitros de vinagre de manzana
\n1 pizca de sal
\n60 mililitros de aceite de oliva virgen extra","Lo primero que debes preparar es el pan. Mientras continúas con el resto de la receta de esta sopa de ajoblanco, déjalo en remojo reblandeciéndose.
\nTritura las almendras crudas usando una picadora potente para que queden muy finas. También puedes usar un moledor.
\nAgrega los ajos, el vinagre y el aceite. Añade el pan escurrido y empieza a triturar. En cuanto se forme una pasta homogénea, podrás añadir el agua y la sal para darle el punto justo. El ajoblanco puede quedar con una textura más espesa o más ligera, depende del gusto de cada persona.
\nUna vez listo nuestro estupendo ajoblanco malagueño, solo queda servir bien frío con unas virutas de jamón. Las uvas también son un maravilloso acompañamiento. Puedes dejarlo en la nevera por varios días y verás que se conserva muy bien.",1);
INSERT INTO recipes VALUES(39,"Receta de Sopa de pollo canaria","Vamos a preparar una deliciosa receta de sopa de pollo canaria, un caldo de pollo buenísimo que se combina con garbanzos y fideos, un plato tradicional muy completo. Este plato es muy parecido a cualquier plato de cocido o caldo, solo que con algunas variaciones. En este caso, lo típico es darle un poco de color al caldo y acompañarlo con unos fideos no muy gordos. Finalmente, el toque de esta sopa lo ponen unas hojas de hierbabuena.","2 muslos de pollo y carcasas
\n1 trozo de ternera
\n1 hueso de ternera
\n1 cebolla
\n1 pimiento verde
\n1 zanahorias
\n2 dientes de ajos
\n200 gramos de garbanzos secos o cocidos
\n1 paquete de fideos medianos, no muy gruesos
\n1 rama de hierbabuena
\n1 cucharada postre de pimentón o azafrán
\n1 pizca de sal","Prepara todos los ingredientes. Lava y pica la zanahoria, el pimiento y la cebolla en trozos, y el tomate por la mitad. Usa los ajos enteros. Lava la carne y límpiala de grasa y pieles. Puedes usar garbanzos de bote o usar garbanzos crudos, pero estos últimos debes tenerlos en remojo durante toda una noche antes de usarlos. Añade todos los ingredientes a una cazuela con un chorro de aceite y una cucharada de pimentón. Cubre con agua y deja cocinar por 40 minutos.
\nUna vez ha pasado este tiempo, prueba que el pollo ya esté tierno, prueba de sal y fíjate si ya están los garbanzos. Deja cocinar hasta que todo esté en su punto. Cuando esté, cuela el caldo de pollo canario y ponlo en otro cazo al fuego, añade los fideos y los garbanzos.
\nRevisa la sopa para confirmar que los fideos estén en su punto. Prueba de sal para rectificar y apaga cuando estén listos.
\nAñade en el plato de la sopa unas hojas de hierba buena al gusto, o también puedes añadirlas cuando estés haciendo los fideos. Sirve la sopa de pollo canaria bien caliente y acompañada del pollo y las verduras. Puedes trocear estas últimas para que se integren bien en el plato.",1);
INSERT INTO recipes VALUES(40,"Receta de Crema de chile poblano","Existe una infinidad de vegetales con los cuales se pueden preparar exquisitas cremas, cumpliendo perfectamente la función como elegantes y deliciosas entradas. En esta ocasión, en RecetasGratis traemos para ti una deliciosa crema de chile poblano, un vegetal económico, fácil de conseguir y muy versátil. Verás que su preparación es muy sencilla y el sabor realmente espectacular. Los niños estarán encantados con esta receta y tú también, ya que comerán vegetales sin siquiera saberlo. Tanto para grandes como para pequeños, ¡es un plato de entrada perfecto!","4 chiles poblanos
\n½ cebolla blanca
\n2 tazas de agua (480 mililitros)
\n1 lata de leche evaporada
\n1 taza de leche entera
\n1 cucharada sopera de mantequilla
\n1 cucharada sopera de consomé de pollo en polvo (o de verduras)
\n2 ramas de cilantro
\n1 pizca de sal","Asa los chiles poblanos en un comal o directamente en el fuego. Cuando estén completamente asados, pero sin llegar a quemarse, introdúcelos en una bolsa y déjalos reposar durante 5 minutos. Pasado este tiempo, retíralos de la bolsa y límpialos bajo el grifo.
\nColoca los chiles en la licuadora con la cebolla pelada y troceada, el agua, la leche evaporada, la leche entera, el consomé de pollo en polvo y el cilantro. Tritura perfectamente estos ingredientes hasta obtener una crema de chile poblano homogénea.
\nPor otro lado, derrite la mantequilla en una sartén a fuego medio. Cuando esté fundida, agrega la crema y deja que espese durante aproximadamente 10 minutos. En este punto, añade la sal para sazonar y mueve constantemente.
\nPasados los 10 minutos, ya puedes servir esta deliciosa crema. Recomendamos servirla caliente o tibia, ya que está mucho más sabrosa que fría. Esta receta de crema de chile poblano es ideal para cenar o para acompañar un filete de pescado a la plancha o de carne.",1);



DROP TABLE recipes_categories;

CREATE TABLE `recipes_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id_fk` (`category_id`),
  KEY `recipes_id_fk` (`recipe_id`),
  CONSTRAINT `category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recipes_id_fk` FOREIGN KEY (`recipe_id`) REFERENCES `recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO recipes_categories VALUES(48,32,5);
INSERT INTO recipes_categories VALUES(49,32,8);
INSERT INTO recipes_categories VALUES(50,33,1);
INSERT INTO recipes_categories VALUES(51,33,5);
INSERT INTO recipes_categories VALUES(52,33,8);
INSERT INTO recipes_categories VALUES(53,34,5);
INSERT INTO recipes_categories VALUES(54,34,8);
INSERT INTO recipes_categories VALUES(55,35,1);
INSERT INTO recipes_categories VALUES(56,35,5);
INSERT INTO recipes_categories VALUES(57,35,7);
INSERT INTO recipes_categories VALUES(58,36,5);
INSERT INTO recipes_categories VALUES(59,36,8);
INSERT INTO recipes_categories VALUES(60,37,2);
INSERT INTO recipes_categories VALUES(61,37,5);
INSERT INTO recipes_categories VALUES(62,37,8);
INSERT INTO recipes_categories VALUES(63,38,1);
INSERT INTO recipes_categories VALUES(64,38,4);
INSERT INTO recipes_categories VALUES(65,38,5);
INSERT INTO recipes_categories VALUES(66,38,8);
INSERT INTO recipes_categories VALUES(67,39,1);
INSERT INTO recipes_categories VALUES(68,39,4);
INSERT INTO recipes_categories VALUES(69,39,5);
INSERT INTO recipes_categories VALUES(70,40,4);
INSERT INTO recipes_categories VALUES(71,40,5);
INSERT INTO recipes_categories VALUES(72,40,7);



DROP TABLE roles;

CREATE TABLE `roles` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO roles VALUES(1,"Administrador");
INSERT INTO roles VALUES(2,"Colaborador");
INSERT INTO roles VALUES(3,"Anonimo");



DROP TABLE users;

CREATE TABLE `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `role_id` int(11) DEFAULT NULL,
    `active` bit(1) DEFAULT b'1',
    `photo` mediumblob,
    `activate_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY `users_photos_fk` (`photo`(3072)),
    KEY `users_role_fk` (`role_id`),
    CONSTRAINT `users_role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES(1,"Super","Admin","admin@admin.com","$2y$10$Sj11x3yoH.y1bh0ul1nkiubW0FZw1YKdYpCaPmS9NDoMR1saH1YHu","C/Sierra","",1,1,"data:image/;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABmJLR0QA/wD/AP+gvaeTAAAgAElEQVR4nO2dZ3hVx7Ww39n7NOmod6EuARJFgCmmGGMwzb0kMe7Yjh3HaU5iJzd2bvJdJ7lp96YnN3FJ4l6xcacX29iA6U30ooIK6l2n7T3fjwMSQl06Tfi8z8ODtPeUdY722jOzZq01ECRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCBBggQJEiRIkCCeRPhbgCBBAJrXJuZrQnkAmA9knr1cCKxTNP2f4YvPHPCHXEEFCeJX5IqR5kZz6x+R8uuA0kMxDcSTEbXRj4glBQ5fyhdUkCBepWFt0rVSiKcUkFLIByPnV6w8d0+uGGluMrWulMh5/WlLIDaE10Zf7Usl6UljgwTxCEKIJwWkSEhFiqfOv9dobPlTf5UDQCKvbIyu+b3npewZgy87+yIiT7+U6nLKkQKZKZGZQlEykTIWiJUQKyAW94vKBFjPVmsBHIAuoUZADVCDEDVS1wsFnJKIQoNROSFS7zrtn082NNxrDh4ccEUhvtG0IenJ8CsrCrwgVheCCuJBZPFzOZpUZiHlpSDygQmaS48WZyeyAgFStpfvZX5rPfuPswp0tgOJONuYADSXjqvwhTpgL7AfwXYV9TORcedJz36ywSOFfFBI8bQECR0KcXZBPpgZjKpLcT/wiKdk7I3gGmQIyNMvpWoueRWwGORsIMnfMp2lHPgUWK2qcpVIu6fU3wJdSMO65AJg7GDqSsGBqPnl+R4WqVuCCjJA7CUv5SuavkSR3CgFPvkjDRUB+3TEu7oi3zCnL/WLufRCGtYlNwFhg6zeFLmgPMKT8vREUEH6gTz+7zTdaPyqLuWtAsb4W56hIOGggnxNMajP+nP90rAuuREIH2T1xsgF5ZGelKcnggrSA1K+oWqFbdchxNeAqwDV3zJ5GA1YiSKeUdNOfCDEE7ovOx8uU6zgIv0CZOUbYXqb/Q5Xke0RIUSuv+XxIipwHbq8TivKPqUVvfhnxSz/KZKWtvio/7UMUkGEZI2HZem5L191FOjIE29EupS27wshHgai/S2Pn6gVgj8rDuVPYtRdjd7sqGl14nhdVfYw8JFZU5ATwhdUHPSGXBfyhVcQWflGmN5qf1giHwVi/C1PgFAjJL9TQvirN0eUhnUj/gbyWwOs9pfIBeXf9YpA3fCFVRAppdCKX/wK8Hskaf6WJ0ApF0I8oaSf+Kc31ijyjXGmxtiaFUgxv59V1kVElV8jpuL0tCw98YVUEGfh8zMF4i/AVH/LMiwQbNN1HjZlLf3c003LN8aZGqNrfo8Q36Dn6ZYG/F9EVPkPfKkc8AVTEFn2VKjLbvl/QogfcPFZpbyNRPKManf+QOTd3+Tpxps2JI3TpbhfwkIh3e7uUlAoJGsU5L98tea4kC+MgjhPvXiVEPIpIN3fsgxziqSuP2jMvtdnliR/ctEriDz1rMWF8oQQ4ocEvZc9hXs0Mbd9X4z4equ/hfEmF7WC2Eteyjdo+qsSxvlblosRIdnvUvTbzRn3+sSz1h9ctG9U16kXb1c1fUtQObyHFOSrUtnuOvX8Pf6WxVtcdCOIlBsNrsLi3wkhfGYrDwIS/mjISPsPIea5/C2LJ7moFERWvhGmtdpeB67xtyxfSCRrVd1yi8hZ0uBvUTzFRaMgsvjVEbru/EDCJf6W5YuMkOxXdK4TOUuLPd12zZzb0hS0P0rkIndnYr3m4rH4z5Yd8XRf57goFEQWvjBGczu/pfhbliAAnFZ1sUBk3+2xB7dmzm1pAtceuroD1UkME2M/ea3EU32dz7BfpNuLnhunwXqCyhFIpGqK/MRx6tlJnmpQQfsj3fvKRSu4vJbIYVgriOPk85eqUtkEJPtbliBdSFCEut5R/NI0TzTWPq3q9h6LPdFHdwxbBbGffH6CooiVfHFd04cDMYqur3UUvjjZy/3IvosMjmGpIPLE86NVRawm6J4+HIhUkKtk4QtDC1UWYn0vd1cPqe1eGHYRhfL4v9M0VWwgcDKI9Juysjq2bz9OW5uD1lY7ra3uBIECsIZZAIiICCEmJoz4uAiiY8KIiwvDZDL6UWqPEK8JVsuS52cONsOK5uIxVeUKus4YaqSiPzp0EbtnWFmx5OF/hesW4yYJE/0ty2BwOjUeeeQ5Tp6s7HcdRVFITYkmZ2QS+fnpTJ2aQ2zsYHMd+BcBBxTNMnuw+yRuM6/r9+etOVZJRX809qPlXks+MWwURO54yqjFh36A7HmxNhw4XVrD9777HG1tg08vm5uXwjVXT2LOnDHDcXRZpWakXT9cdtyHjYI4T73wZyF42N9y9Iam6RQVVXPkSCnHj1dQXd1Ifb3b2dVsMRIXG0FKSjRlZXV89NHQ/fuio63ce9885l85vj3j4nBAIv5gzLzba9MiT+LVb9VTO5+uUy/ejpCveEXIIaLrOvv3FbNhYwGbNx+htdXucxkuuSSLxx6/mTCr2ed9DxopbzNk3fO6v8XoC68piKd2Pu0lL+Wrmr4VCPW4kEPA6dRYv34/b765hfLyen+LQ3pGHL/61R1ER1n7LhwYNGtCnxHorvJeM/N6YudTnnrWYtD0lwgw5fj00yM8+LUn+etfVwaEcgAUF1Xzh9+9j9S9tiXgacIMUnldlrwR4m9BesNrCuKJnU9NKP8rYYLnpBoa9Q2tPPHEMn796+VUVnk1bdSg2LX7FCtW7fa3GP1GwjiXZvulv+XoDX9tFPb5mnOeevEqEAPNmeRVjhwu5XRJjb/F6JW339qGlMNmFEHA95xFLy70txw94T0FGcLOp6x4wSqE/DsBZmWbPn0UTz79IDfcMMXfovRIeUUdxUVV/hZjIAgh5TOy8o3BZnr3Kl5TEM3FY0BdN7f63PnUbPI3QJZXBBsC9Q2tPPmP1XzwQWBPY06eGlYKApDharX93N9CdIfXFCT+s2VHJIaJApYBjWf/vSEVfVJvO5/Owudngvimt+QaDE6nxptvbuVrDzzJypV70HWfJkIfMA2Nwy/RiICHPeX560m86ot11pS7pL/lpXxC0QrFnxGB40TZ1ubg4Yf/TVlZd4NhYKKIgPn6BoKq6NpfpJSzhBABs4gKqG9SK8y5B0FAvUVCQkzMmTOoLP1+w2o1+VuEQSJmaEUv3eFvKc4nYBREVr4RhpABafKrrvZ4pk2vEnbWM3h4In8jy54KmH2vgFEQvdX2XQIwMvCzzw6zbt0+f4sxIEJDh7OCkKo7LAFj3g8IM6o88UakptpOEmABUDU1TXz72/+isbHN36L0GyEEJpPK9ddP5dZbZxEaGpj+WSIkBSV2BgB6zVZkW6cwkRrV5szyRpLsgRIQI4hLafs+AaYcADabk2nTRmI0Dp9E8EKA3e7izTe38oMfvkhzi++dJ/uDEjsD1FBQQ1Fipl94O1a3GAPCc9vvI4iseMGq2SgCYv0tS0/U1DTxta89id0+LEIYOrF48SQefvhqf4vRBTX1y24FAXC1oJUuv7BIrWoh3YdnJnaL30cQ3c79BLByAGz9/NiwVA6AjRsLcDgCT3a9ZitoreBqQa/d2l2RGN0m/Z7z168KIuUbqpSBHQTldGose32zv8UYNA6HMyCtcLKtFO30W2ily5FtZd2XQTwq5Rt+nd/6VUG0wrbrgBx/ytAXa9ftoyoAH7CBMJyiDS8gWyu2+TXPsn+nWEI86Nf++8GV88azYL5Pzqz3GjExAekH2D90+YA/u/ebgsjTL6XixYx4nkJKyefbjvlbjEETFmbBbB522Z06EOIaWfzqCH917zcF0TX9qwyDgzT37S2iqcnmbzEGzbAePdwYdM3pt8W6/xREcpu/+h4I5WcCI6R2sMTEDnsFAbjdXx37RUHsJ5+fIGBoqSh9hDMATaTnMyKh99TEcTHDM8nc+UhBvix6yS8eo35REEUR/XaB9zehQ0ilYzR6d+6fmRbP126d22uZpOSLI7e3jrzFH/36R0HgRn/0OxgiwwfmWBoWambh7Hx+/v0vMzIjwUtSQVpyLI8/dD25I0eQEBPRY7nk5CivyeBTpPTLM+Nz84Y8/VKq5tKHzcmzEZF9Z6UxqAqTxmUwZ1oek8ZlYlAVTpVUcuh49xtgvaEIheyMeI4XnumxzJiRKTxy/9VYzzoiTp2QzYqP9nRbNvkiGUEkTJLFr44Q6bcP/EsdAj5XEE3TryYAfMD6S1xcz29ns8nAHTfMYsbkUYRbO7uYr/xo74D6EUIwNT+bJddOZ+eBU90qiBCCRZfnc+dNl2FQOwb/L4KCAELTnIuA53zZqe8N5FIs8uJ5Jx4nKSkSk8mIw+Hscm/ahGwWXt6xidjSaqeopIrSiho27+x+78QaYqalrbOH7YS8dJZcN53sNPeU7MW3N3WpFxpiZnp+NsmxERQcLCY5KZq4uAgUIcjNTiIyIrRLLLo11EJEREDnZRsYgsVc9AqCnO37PgePoiikpkZ3e2TB7Gm5ADQ1t/H7v79PaXktUkocSPQLclMZEKgCIq2WdgUZnZXMrddNZ8zIjuMVn3/9Y/Yf7pyV1SgE2Jx8vv0In293X1MVwc8fv43E+Cj36DM+i/WbO2fxTB5xkaw/2pFzfN2jTxVEFj+Xo+nD7+CbjIz4LgoSHWllfG4qAOFhIeSPSUNRFEZmJbFu60Gky8WozGQmjkknOy2OxqY2yipq2br/FBkpsdxy7Qwmj8vs0ldmRhxyM4SYTUzITWXm5FHExYShKio2u5OWVhtlFXWYTQYS4zsUYOqE7K4KcvFMr84iRshTz2aKrPsKfdWjTxVEk+plw2l6dY6Ro5LZuPH8h08wbdp4TtYJmhw6TXaw5F1K/iiob2zlkthckuMiiLEaiDQLwkMFY3IFDY3NpKTEMXPyqB4dCDNSE/mv736JlJRESpoE9W2S405JUwsoCmAC0jMwmWBziY7VBNEhkJ2VSqjFTKutY/qWMuJiUxDQhOEyoNBX/fl2iiXlVJ/25yHGjklFMYUQNWYqpsg4TBHRHFEMHNnd3SaiBbBQdgagI3+WSYX8hBDmjR1Nb861bZYYNlXqFBZqDDSDaOKCO2itqcRWVUrD8T2kpAZ0mM2gkFKfBrzsq/58qyBCTBzwXz0AyM5JREgn4Wm5Z1/jbjIiBbPSVFLC3aGuIQb3k79lxzE+3LgPa3g4c+ZOxW6J5HC1ZGe5zt4zklvHqYxP6KwluoS3D2nsKNcRAnKiBdlRCqKlhtWrtoLm4vpFk7hkfBaaLnHo4NDgeK3koyKdFodEV4xY4lPQXW6DQupFqCBCCJ8mM/ftRqGUw8pv3GZzUl5RR2NDKyFmI4766vZ7Y+IUvjbZwIREQWyoICZEEGIEg9BoqKulpaaashMn+PD195ifpvHITANfHqMikSw/5MJ1wXvieK3OjnKdmBDBd6YZuP8SA/OyFGblRnP5xBSqyit4/qU1NDU0EmZ295cUJpidrvCtqSqh553EZq9zm4gtISbKK+pwuTRffF2+wqcK4rMR5OwGYcBPiqWUfPzxQd5atpXComp02TFNMlSVYIpxm2KvH62gdvN6MRpUbr5mOnX1LWzedoSm5jaOnChn4rgMpo5QKGmQbCvTef+IRoylYxQ5VO3u50tjVJLDO66bjAZmXZrL8g8/R9N09uw/yaJ5kzr1GR0iuCJDZeVxDSEltmr3Xto3HnraLZNRJXfUCG674zIuuSTgUh4PlFhZ+GKyyLy73Bed+UxBXE45MtAD26SU/PH3H7B+4wHiosOYP2sssTHhOJwuCk9Xs+vALkIS04hJSCA6pPcPc/6eROt5B3ZOSRFsK4NtpW6FEHSYLSwqZEd3tFtaUYNRNbBp66Fu2zqf9Eh3vbyQGgrrq4iODOXKWeMxqgplVQ3sPnCKn/zkNe6843LuuHNYWdq74FIYCVxcCiICMFv7hbz99jbWbzzAwtn53HXzZRgNHeEqLW0Ovvb4M0S4alFEzz5WR06Usfz9rZw4uxMuhCAzLa79/ogwBSE08hMVvpynYlSgzg7/2O4iOVx0cjHQNMl//abz0YxZad33rShuNVswLpbVQjB+dBpfvqoji2ubzcE/Xl7Hy69sIjMrnlmzcvv9vQQaQspMoOtuqhfw2RpEIjN91ddgee/9HaSPiOPer1zerhy6rgESa4gJs8FIerSZJjtd1hDnyMlM6mTCnXvZOJITO2aWBgXMqiAhxG3ZEgJiLGA1QvQFjsPpKXHMnp7X/vu4vDQmjM/ott/6NoFBQHKUibBQCwmxnV1kQiwmvrN0EZFhIaxYEdjHN/SFL1+2vhtBFNIDfQvEbnOSm5vc6QE/U12LqihYrSFYLEby0iI5WgaNdveDfSEGVeEH376RvftPERpqJm9USqf7UoIuJaHGzu8miwGc3Xw/99w2l0snj8LhdJE/Jr3H/ZN6uyQm1D0CRUdZiYsNBylptdkxGY0YDCpGo4HwcEuXXf7hhpR0/5bwAr4z80rifdbXIImOCaemvnMGE0UIDKpKc3MLFouBS3JiWXVGUtygE2PpfgA2qApTJnWfrKXR7jbPpkR0ftATrYKK5q4PrhCCsWd37HujsF6ScnZxHxtpJcRipKKqGl3XiQwPx2Bwu+1X17UwKs9vId6eQcq4vgt5Bl+aeX32oQZLRlosRadrOp3xpyoqJrORhPh4Qs1mLCYDuXGCA5WDewsfq9UxKnSyVAGMjFEobZa0DiKAsc0JJ2p18uLdbVosJsItRiLCw1EUBVV1Txcraxux2RykpgX8n6J3hLgoFSTgd61GjU6mtc1OeVVHHLqqKmguCVK2T29mpqgcqtKpHGBSTAlsOa1zSZLCBTMsxsQLQgzwecnAT6/acloj1Ahj49yNGlSV8PBwQkLMSF1HPWuPPlnk9icbPTrgkugPCOnDZ8mXCtJzYEWAMHq0e+px9FRF+zVVVdF1DU2X7Q9aVrTgkiSF1wtcuAZwLvnHhTq1bbAgp+vXblDg6pEqHxVplHUz1eqJ0kbJxiKdG3INGJRzMgtcuo6u6ciznwHg5OkqhBCMzE7sd/uBiIBIX/XlSwUJzDz855GbO4KQEBP7jhS3XwsPsxIdFYmm6RgMHV/XTXkqYSbBM7s0Gu29P9AuCWtOaGw4pXHXBJVwU/cL7cnJCtNSFJ7d5eJoTd8jydEanWf3alyTozImrqNNg6q0K4fFbEY56x6z52AhGRnxREQGzPk0g8Vnz5IvfbEC/lwwo1FlwsQM9u4twqXpGFSlfVrl0vT2Bw3cb/x7Jqp8Wqzzf9s1RsYI8mIVoixgNYFLh3qb5FQ97KnQCDUKHppmYERY7xuM145USbIqvHbARVK4Tn6CQnKYINzsrtdg16lugT0VOm0uuHO8SlZ05zZVVcGlaRhUlZhot0t8dV0zp8tquXXJLE9+Zf4iqCD+YtrUkXy+9RgHj51mQl56+3VN1zqFuQIoAuZkKMxKVSio0jnVoFNRKnFo4NQgyiJIsMKt4wxkRIpevXjPIQRMHSGYmGTkQKXOyVrJ/jM6LU4wqhBhEowIh4U5KllR3TeoqiraBSfxbt97AoCpUwM6FXJ/uSgVZFhw2WW5PP30WtZvLuisIC4dVenJrAsTkxQmejAUzKjAJUkKlwyiTVURbsPCWaSUbNhykKTEKPLGDHMTr4/x5RqkeyeiACMiIoQ5c8ayc18h1XXN7dc1XaJ0550YgKiq0mkEOXD0NKUVtVxz7eRO08RhjM+OzQoYBRGhaSiJi1DTb0dNvx0lcREipO8NMm9ww/VTkEhefGcT2lkrlabpGDz0cL27cjttts5fh93h4oM1O3FpAzfzXoiquNcg4PbBeuX9LYSEmFi40Kee4t7EZwriyylWjx9KiZ6MiOicKktYEhGWRGTDAfR63/oO5YxM4uabL2X58s/5edNy7r9lHgZVdLJiDYW5l43l3ZXbaGtzEhJqoq3NgcloYPGVE7uscwaDwaCi6zoHjp3m3298zJmqBr73vWsupgwnF5+CSCkbhOjqBitC07ooR6f7keMR9ipk22mvynchX/3qPGJiw3jpxU/4z/95nSsuG+Ox6UlkhJXbbp6NpktaW22Ehpjb91g8gZSwatM+Dh0tIzrKyuOP38Rll+X1XXGYIMFnGcV956woRDUwqsv18L5zWIuIMT5XECEEN990KTNnjubv/7eaDZsKyM3x7AJXVQThYZ5/q+/Yf4KTxVUsWjiBr94/n/DwYX1uehcEVPddyjP4cg1S091FYe7ba0CY/OelkpQYxRNP3MLo0clUVNZ1ijAMROobWzh1upr58/P57veuveiUAwDR/bPkDXyoILLKd315FkVRmDcvn4amNg73I9+uS9PZsv1IJ6fHobJzz4kuC/vu2L73JFKXLFgwrML/B4a8CEcQKSnu9rq975eBdPjshdEjl1+eh6IobN1zos+yBlVhRFIML7/5CUdOlA1JUU4WnuHlNzcRHhFKiKXvvdbNu48THW1l/Pi0QfcZ6Ahkka/68mHIrSjs7rpsPIiw9O48JxsPekOkAREdbSV/Qjqbdx7jK9dcSoS197VDRlo8qSMuZ/P2o2zcdICkhChGZiWRkRbf67qjucVG8elqThRWUFpeS+7IEdx640wM/Thr5GRxJUdPlnPDDVMvlv2ObpGi+2fJG/jOioUoFN2EFMq208iGA4jI8d3Xa9iPbCv1tnj94s7bZ/MfP3qJt9fu5J6b+k58oKoKl8/I4/IZeRSfrqLgSCkbNxdQV9eCy+UixGLCZDLidLpotTkwqCpREaHkZCYxLi+V6xcPLM/e6yu2YjQa+NKXLh3sRxwWSDjlq758piAGg35M07r3HdLrdyPsVYiIMQizOxZG2quRjQcDRjkAxo1PY/qMUaz7eD8zJuaQm9X/uIr01HjSU+MBd8oeKSXNLTZsdicWs5Ewq2VI55lv/Pwg+w+VsGTJrF6PbLgYMEjR9zzXQ/g0EY+r8IVqhkHgVG/U1bfwnW//C0UIfvWDJYT3MdXyBacravnpH5aRlBzDn/64FJPJ2Hel4Uu1IXOpz8K3fT1RPeDj/jxOdJSVRx65jtr6Fv743Grsfj7ks7ahmf/954coisJ//vjmi105ADmwk4mGiK8VZJ+P+/MKkydn842HFnH4aCl/eH4lTj+l9mxuaeO3T71PXW0zjz12MykpMX6Rw5dI6duXrG8VRLDdp/15kWuvncwdd8xm/4FifvvMh5RX1vm0/8raJn7x93cpLa/n0R/ewNSp2T7t318IIbb5tD9fdiaLXs7WpOazBZYveP2Nzbz4wicAXD//EpZcNx1FePe98+muo7y0/FNsdiff+/61XDHHL0eI+wVVI0PkLO12T80b+DxbrqvwhTJgeKfVuIANGw7wpz9/iObSCQ8L4e6bL2P6pJGdUpd2x5pP9nGmppGbFk7pl09WRVU9f3p2FcWlNcTGhvOfP76Z3LyUPutdRJQaMpf6NAbCZwpi33FnvqLIB8B8LzjddkglBGGKA1MqGMJ9JYpXqKis50c/fJnq6kYAIsJCmD0tl2kTshmdldTFhLt81XbeXOmeLeRkJPCjh24gLLRrJKnN5mDXwSI+2XaYfYdLQErGj0/n8R/fTNTwT74wQOSrhsx77vBlj15XEHnsarPWFPlHKcXX6WnNIwTCnA7WvB6LDAc0TefRR1/g2LHOicctFhNZqXHERoWhGhROFlVRUt7ZfSYqwsrkcZlER4Yigdq6ZgrLqikprXZHMwr3ITtLbpnJ0qVXIJQAT5XvDaS815B1z/O+7NKrG4Xy2NVmV2PUSmBe7wUl0lYEWjMiYirDVUlUVSElJYbTxdWMSo9j31lFsdkcHOrDybG+sYUNWwq6vZcQHcbUcWms+PQQc+eO/WIqB0hVKGt83alXn0StIfpP9KUc5+OsgZbD3hPIBzQ2thEeauZ7d83hm0tmYQ0ZfDIXIQRzp+bwxDcXMyLBPSu12bqe1/5FQMBuXx2acz5eG0HsO+7Ml0J/cKD1pL0YYUkHNcwbYnmdpqbWdqWYOi6NURnxvPD+DnYfHpjLTGJsOPfeOJXcDHcQ5rk2z1Q1ftEW5meR7/ijV6+NIO4F+SDalxJsvo0e9CR1dS1Yz1tsR4ZZ+M7ts3ngS9OJDOs7eMloULlx3nh+8c3F7coBkJ4UhRCCPbsKvSF2wKMg3vRHv95bg0i5YNBVXdW+tz8PESkl77yznerqJq6YmNnl/qyJmUwZm8qqz47wwScH0brJXjJx9Ahuv+YSEqK7jp7REaHkj0xi7dp9XD4n72I4a7DfCMQekXn3ob5LeqNvL+HceXsTMLh5klARMYs8K5CXqKtrYd0np9m4ahNFxVVkp8byw3vnYu4lfuP0mQaWrd3DgeMVSAmpiZEsWTSR8SN73x6qrmvhl/9aT1OzncnTxnDVojymTcvB0Md+y3BHSB5Xs5b+xi99e6th587bG4HBbW4IAyJmoWcF8iA2m5MtW46yYf0B9uwtRNd1YuNiuGJSGotnjupzg/Acp8/UU1XXwqTcEf12da9vsbN8WyXbtuzEYbcTHmZhzpyxLFw4gVHD/FiDHnCpijFDpN/ed6yzF/CmghQAg/OBMIQjIgPrJFapSw4dKmX9un189MkhbDYHIdZQJs+azaVzr2DSjOkotkZcRzYgm7wTfi/C41FHX4mwRuN02Nm9eSsb3n+Xg7v3IKUkJTmGK+aOZf7CfJISo7wigx94x5C59GZ/de5NBfkT8N3B1BWWTLD2nQ7IFxQWVrJ+7X4++vggtXXNGAwqE6dPZ8b8BUy5bBZG04W73xJZU4hWvBPZ5JncAiI8HjV9CiI2g+7+ZNUVFWxes4LP1m2grKQUoQgmjktn0dUTmTkzF5NpGKdgFvJ6Q8Y9H/ite281bN9+63hFUfYAA5sgCwGRsxF+NPPW1DTx0ccHWb92P0XF7tFg1Lg85l93NVdcvYAQawRNzX3HgcimavSKg+iVR0EbYJzvZ1QAABycSURBVNyIoqLEZqAkj0VEpdDXnyo8zIDRqHBwz37WvbeCTavX0drSitVq5oo5Y1m0eCKjRg2vKZiEo4aMk2OEeMJvuZa8aixy7bzjbxL5rYHUESGZEOr70aO52caWLUf5aP0B9h0oQZc6CSOSuPK6q5h3zWLSsjofrNrU7MTh6N/fTbocyJoi9OqTyLpi0HuIH1FURHQ6Slw2SmwGGPq3yWgyKYSHdQ6UstvsfLZuI6vf/oADO3cjpSQ9LY7FV03kynnjh8UhOkLKh9Sse57yqwzebFwW3GJy2YwrQM7vVwVjLCJimrfF6sShQ6d5/dXP2L2nEJemExZm5bJF87nyuqsYP3lij4tnXZc0NDrRB3AEGwCaC722GFl9AllfAoCITkfEZqPEpIM6sOmQoggiI4wovbiflJ8uZd17K1j3zgdUnanCYFC5dGoOi66ayJQp2YGaAaVKVS0ZIm1Jmz+F8L6zYsEtJs2m/l4ivkFP0y0hdGHJUAjN6yKS1nIIe9VyBAJTwpdRQ3M9IlfFmXr+/rdV7Nx1CrPZzOxFVzJ74Twmz7wUo6l/b26HQ6epeSiuH9J9sucQkjWEhxkxmfr3gEtdZ/fn21nzzods3fAJDoeDuNhwFizIZ9788aSmBE66ACH5sZq19Nd+l8NXHdn33D5O0cT9IBcCmWcvFyJZI82xryrhl66ArmeptxX+Ct3VAIBiiCQk88dDlmXNmr089fQ6XE6N6277CrfcdzfRcYMLVx3IVOtCWlttICA0ZHDpQY1GhYjwwcWgNzc2sfHD1ax48x2Kjp8EIH9cOvd9dW4guLJUqzZntsi7v6nvot4lYDastVMvPCYFXd4YnlQQKSVPPbWW99/fScbILH7wy/8iJ2/04IUGdB3qGxwDzp7ocOls3FOGLiXzL0nBNMCjFYSAqEgznpgdnTx6nPXvrWDlsndw2B3cdtss7rzr8qE3PEgE8lE1854/+E2A8wgYBZEVL1g1mzwKolMKda31CI7KtwCGPMX6+99X8+GHu5izeD6P/vdP+z2V6gu7Xae5pf9TLSlh86EKKmrd0+uEqBBmj0scUF6sMKsRs9mza4faqhp+/5Ofs3vrdpbcMpN77p3r0fb7haBEVSy5/l57nCNgFATAVfTiUqT0SkDMqlV7+OtfVzJ38Xx+8JsnUBTPumcMZKq1/1QtR0sbOl3LTY1kfGb/pnlDmVr1hcvl4hffe4wdn27hksmZVFc38fvfLSW0m2jHC2lqsvH97z3H6NwRzJg5ihnTRw48DZHkdkPW0tcGKb7HCSgFkVIKrejFLcB0T7bb1GTjgQf+QVxCEn9+/TmPjRzn09+pVllNC1sOVXZ779LcBNLirb3Wd0+tTL1arQZLfW0d773yBiuWvUtjfccZNXfdNYfbb7+sz/ovv7yJV175tP13a6iFRYsmcMNN00iI71e2xy1qxt2XCSE8lxZ/iASUfU8IIXXJdwGPbgytXLWb5mYbD/7o+15RDgBFAWto76NSQ4uDbUd6dkPZeayKuubejziwhho8rhx2m52X/vFP7rv6S7z2zPOdlAPgtdc+Y+fOk722sWvXSV5/fXOnay2tNt5+Zxtfe+BJ/vnMepqbbb01oelC/04gKQcEmIIAmLKWfi4lf/Nkmx9tLGBEeiqTZkzzZLNdMJvVHk2uQgj2nKxuPxS0OzRdsudEdY8KYDQqmM2e99zds3U7rzz5b+y2s0f/CYWM0VMYme/2h3O5NH75y7d4//0dOC7IJGmzOXn55U384hfL2134R0+YQ/a4me3TWJdL4+13tnHkSM/+hhL+ZMq4d6fHP9wQCUgnHUMIP9ZsXAcMORtaYWElRUVV3PmN+z0gWd9YQ404nV2nWhHhBjKTI6hu6N2RMSclgrAwI42NTjgvG74QEGb1zp9r8qxLCQsPo7mpGdVg4tZv/5GIqASk1HHYWyk+ugu73cWTT67lhRc+YdSoJMIjQqg808DJE2c6ncybPuoS5t38LYRQaGmq5ZU/fwfN5SA83MLESZk9iVBkCLU84ZUPN0QCbgQBEElLW6SufwO6OS9hgGzb7s5Td9mC/ofGD4XuplrWUAMGg8K4zKhezbkmg8LYzCiMBtFtG95YdwAYTSZSMtIB0FwOrGHRAAihsHjJD7hk9k1wNhlea6udvXuL+HTTYY4eLe+kHGOnLOSq2/4DcbZsSGgEuua27iUnR/d0gq+UQj4gEpY0d3fT3wSkggAYs+9dI6X8y1DbKS6qwmQykZ6d0XdhD2E2qxiN4uzPChaL+2E3m1TmTEpC6cacqwjBFZOSMZvcZS0WtX065a2p1flYQjsS1zkdHWsF1WBk+oI7+fIDv2LMlAWEhHV2o4+ISmDMlAXc8o3fMef6B1HOc5VxuTpGUmMPHsUS/mDMuGedJz+LJwnIKdY5DK6GH+nGqHkSJgy2jeKSGlIy0zxu1u2LMKuJpmYn1tDOZs7sERHUNzSz/XAVUrjvCelkcl4CWSM6x5dZQ1V0XXptanUOKSXlJe48AGaLFUto1zi3+JQcrkjJ4Yrrv47mctDSXI8lJAyTuWenR5M5lBBrBG0tjZSV1iJ12SllkYADqtR+4vlP5DkCWkHEqIft9qLn7lCl8jnQu/2zG6SUnC6pZvq87k+v8iaKApERXfcANu88xsvvbe7i5Lhvj4L9xpnMnNxxUrYQwmv7Heez5/PtVJafAdyK0BNSgt1hx+HQQA3D6VKQwoHJaOrRnSwhZRRFR3dSV9fC1s+PMXNmu+dCkwJLRNZ9vZq2/E1AKwiAOePeAlfh83eDeIsB7tu0tDqw210kJCd5SbqBUVnTyKsfbO3WA1jXdV55fys5GYkkxPruhKiW5mb+/usOr45xUxd3uu9yaVTXNlBX30xzcxua3tUCbzAoRIRZiYgIJT42CvW8tcb46VdTdNRtnHrqyTXk5aUQHW2VCO4XGUv9kohhIATsGuR8DJn3vC0RfxxovaZGt7dCRGRgHEl2qqQKVy9nibhcGqdKfHdadl11LT996PuUFrqTpSemjiIrz30uotPlorD4DLv2HaOw+AwNjS3dKodbbp3a+iZ3+b3HKT7d8TnTciaSkuUewauqm/jx4y+z/0DxPw0ZS5f54CMOmYAfQc5hyEj9kVZUMha4qr91mprcChIeIApSUd3QZ5kzNX2XGShP/vYPxMbHkz9tMrHx8TTU1bFj0xaWv/gqzY1uh1lLaAQLlzwKQqG2rolTRRU4XR17HramChoqDtPaUIbmbMVla0GoKkZLJCERSYTHjyQsJhNN1yirqKaqpo6RWSOIjAhj4S3f562nH6epvpLikhoe+9HLdy8eN8YBygfoeqVU5QiBMldIPWNVweEleMB66SmGjYIIMc8lD/9riW4xfSKRk/pTp/HsCBIWERiZ4yuq+n74+1NmIOzdtpP3Xuk951pYRCxX3fEYYRGxlJ+pofh0JW7jk6Sh4hAVRzegCMgcPZX8SddjDY8mxBqBrbmBluZ6SouOcKrgA1yaRs6M+zCFROJ0ahw+VsKIpFjSUhK4bulPWfXqb6mrOg1gAb4F+rdQQEgBSCSCb98w9Yd/e2/H/3j0SxgCw0ZBAETe/U2y5PnrNE1sBfo8J6Kl1b3+C4sIjBGksj8jiIcV5JUn/93zTaGQO/EKpi+8k1BrJCWlVZSWuxNNOG1NFO1+A4MC8298iNSc/C7VI6Ld59tnjbmU2VfdTU1FIS41nJLSGhx2G4pqpLS8BhCkpSRx8wO/ZOfHb3Fg2yo0V/cuNW2t2q/aPr3ltZDZy3x2SE5vDCsFARBp95TKEy9fqanaJ0Cvq2/nWbcIs2VwAUmeRNcllbV9x/9U1jSh69Ijm4JHDx5i/87dAIRHxpGSPQG7rRmzxUpMYgZZedMIj3KnN62uaWxXjtaGMop2vsr46Vczdc6X+u2GH5uUCYBFlbz19A/JmHIb1uh0SsurMRpVkhJimLnobibP+RLFR3dRc6YIu60FS0g4xcfcv58ut6kfbW98GfBfQMp5DDsFARA5dx6zl7y0SNX0j4AefcSdTvei0tBLlkNfUV3X1OsC/RwuTaOmvpn4mKFPC9e+82H7z/kzrmHCzOu7LWezOzhV7E6cbm+ppXD7y8y98SGyx1w6qH4jY+JY8JXvsnbZn8m+9C5CIkdQVFJJeFgo1lALZouVURMuZ9R5OhCblMG6N/8EwLa9DR715h4Kw8KK1R3mtLv264pyFVDbUxmn0z2CeMuDdyBU1jT2v2w/pmJ9oesam9ZsAEA1mBg9cW6PZU8VVaBpOrrLTuGOl7l0/m19KofTae/1fvrIScy98UEKd72OdnZH/VRRBT1FA2SPmd6+Qbltd6NxwZTsyF478BH+f7UOAVP6Xdtl0UuXa1Jbe2EkIoDT6X5jG43+Pzu8oqq+70LtZRsYN3poR/EdO3iExjp3n7rmZP/nK5g6d0mX6VJLi42GxhYAzpz4lKS00Yyf1k1eZKlz/MBmDu/ZSEXxYVxOB6rBRGLqKEZPvILcSVe0+2CdI2fsTFwuQZ3dfb25pY36hiaiozqPjlJKtm14DVurewrqckkUzXQ54LeEcecYtiPIOUTGXQdVXbkSQcmF9wJpBDlT3f8RpGIAo01vJKe5ky9IKdn58ZtsXvVslzKlFR2L8tqSHcy+6u4uZWytjbz3/M9Y99afOX1iHy6ne4GtuRyUFRbw0bt/Z/k/f0xLU9fBPHfCDOLjO7KlVHRzXPbmVc+y57N32383GZT9QhN+ycV7IcNeQQBE9t1HVCmmC9h1/vX2EWSgYZ9e4MwApk2VHrBk5Y4fy9Pvvsa9D3+jfdTY//lKCg93HFUvpU5dvXv0qD29h6wxM7FGdE7943TYeP+Fn1NWeLD9WkJyMvlTp5GcmtZ+rar0BO899zMc9tYusjSW7qH8sNsfsbGppdNarOT4Hg5sW+X+RSAT4yz/7/29BRNXHzq0q0tDfuCiUBAAkXl3uRJquYLzhuWOEcT/ClIxkBHEA2sQAFVVWXL/3Xz7P3/Yfm3bhlfbf25qtiGl25DRVHmY3Aldw2p3bHydmooiAGITEvnl08/wwtr1/O9zz/PsqtX873PPk5jiHqkaasrYsubFLm2kZubSVHUUcPtznZvSAWzf+Pp5sTPimy98vPsXBNBG4UWjIAAiYUmzmpF2s0T8AdwuEOD/NUirzUFTc/+TdDQ2t9Ha1nvo7UC4+pabyJ/i3lutrSyhsvQ4AE3N7re95rRja64iOaNzMn6HvZUD293nZpotFn77738zZVZnJcqfOo3f/vtZQsPcuZSP7N5IW3Pn9Vb8iCxc9macNvcao6XV/V001Fa0yyJgx+oDB5/02If2EBeVgoB7x92YefejSHmT3e6eLPvbzDuYEcHTLifzru1wQqwsPQZ0jLBOWyMWa3SXFKSlpw60b+gtuvlLpGZ2f6pVUkoq13zlFsBtPTt9cn/nAkLBGhmPo82tODa7q5McABL8csRaX1x0CnIOQ9Y9727deux9IQR6N8ed9YbD4UTvwTFvMAxmd9zTO+rJaR3rhcZat2v7uRhyp72ZEGtXq2pT3Zn2n8dO6t27Z+wlkzvaP6/eOULDonDZ3UGD+tnk3eePNBLlRJ8fwg9ctAoC0NDQekBKybeWLJUFu/f1u159fRNtbb3b+QfCQBbo7XU8OIK4XBqNzR2L53OL9nPeuaGRSYybflOXeuK8IDOXq/fjG/TzMtZ3t/PufuG4r6tn2/XkS8hbXNQKsurAwZ8B95QWltT/x33f4Hf/+fP2vYFekXh0mTgQE+85Kqo8Y+o9R1VFRfvPlrOjxbm4DdUYgjUmrUudyOgOT57dW7b02v6uLR0pf6Jiu2xJYWttwmC2duo3PLIjFbMQcmg5YL3ERa0ggFxdcOgFoTNWSvnihg9W8cANt8qVb74z4Fy6Q+FMdf83CYdSpzf2bP68/ef4Ee5kMRZzR7ZEm61r6tSU7PHtIbUfr1xBwa7uLa/HDhaw5u23AfeuferIiZ3u67pOU205FqvbhHzOaBI3IrtjtJHyVgIskSFc/AoCwIqCgorVBYeWSuQNLc3N5X/9xf/w+Nce5vD+Aq/3res6VbUDT9hRVdPssSnI/h272PmZewQIsUYwItMdwBRm7XDiPLj17XaL0jlUg4lJs28EQNM0fvLQg6x44w3sNreXtMNuY/Xby3n8/q/icroVbMKMa7vEqVeVHccUGolqcl+PCHf/HxmTRFJ63rliExaPGzOoI/u8ycV9fvAFnKiqPjo2OuYZlyK06ooz01Yvf9944vBRMkbmEBUT3V6upaUNk8k48Lyy3VBV28SGzQf7LngBupRMn5SDtR85cXtjz9bt/OJ7P2p/gKcvuIOkNPdsxmwyUHGmDikltuZKKgr3MGr8rE71E9NyqSg5QlNdJS6nk88//oi3nnuWVW+9yfN//QufrVuLw+G2dCWmjmbeTd/skiBj67rXEZYEwmIzEUIhO7MjUXd4VDxH923i7Jx2cU5CnP3uqurNHwXIXsgXSkEAjtTUOE5UVW/Mio55VigisrSwZNKKN98RZcXFZI8eRXhkhEcV5GRJJdv3nRpU3bEjU0iMG1wsS01lFX9+4jc895d/4Dprzk3NmcDlV3+1/cAeIQRtNgetbXbM1jhO7XmPkfmzMFs68mMIIRg5biZN9VXUnnGHaOiaRktTE7rWsTDPGTuDhUsewWjqHFrQ2lTHpyv+Rfqkm1FUE7Ex4cTGdFjMIqIT0FwOKooPu7tDLDidEP+VkQmJO05UVZUO6sN7kC+cgpzjZE1N04mq6vdHJSS8iZTpp46dGP3h68spPH5ShFitJKemYDIPXUH2HS7h0InBuRWlj4ghOz1hUHUry8/wj1//vv33jNFTWHTrD1DVzp/JZDJQWV2PohqQUlJUsIncSXM6lVEUlewx00nNcR9Jp7ncWR/DI+PJzJ3KZVffx8TLbkQ1dP2+Vi/7E+aoLCITcxECRmalYLxgXyolKx8hBOVF7SNtghRy2YnK6t4TAvuAYe3N6wlWFhQcBG5YNGbMeE24Hvls3ca7Pl2zwZiakcZ1t93C/Buuwho2+BN3zwzB8XAw1q9zZI7MJmFEEpVlFQghmH3tAxiNXadrYdYQYqLCqa1vIiF7Fsc3/5Odn7zDlDldzb5JabkkpfX/fJbSwkPUVJYxevbXAYiMCOv2GAUhBBNnXsfeze+dS1rXpNqcn/S7Iy/yhR1BLuREdXXlicrqd0fHJzwzfsolk04dOx63fdNm83uvLKO0qBip68QnJg7Yr2vtp/upbWjpu2A3mI2GTnmyBkp9XR0Fu/YCYDKHtGcXuZDw8FCqqhuQQETCaA5ufg2TJYSEET3nyOoLp8tFSXkL4cn5CMWAQVXJHZmGwdD9I3d413pOnXOkFPKVVYePLh905x7kCz+CXMiKgoKKFQUFC+dmZlpMVstCh92+ZOOHq29Z//5Ks6oq5OWPZ/aiK5l55RUkJCf22d5QRoHBur3bbXYO7dlHY32je70hJYd2rMUSGk5bcz2mkHCi40aQmp2PajBhMhrIyRrB0RMlGC0R5My4j+0fv0T1mRJmL74LtZ/HUYPbBX7LutdotgmiUqciFCNCQHZWMuazU1aX00Hh0R3UVZ52y2MJ5dShDjO0kGovgfS+JeDszgOhZs5taQraHyXSHeEjxHrNxWPxny074sl+5k6aFGV2OG4WQt4mFGWurusmgITkJEaOzWPU2DxGjR3NyDF5RER35K5tabPzg1+92mO7/eF3P74da0jvlqyqikoO793PoX0HOLx3P8cPHcHl0hBCEBJiorW1e68AozmECTOuZdJlN2I0WaiqaeBkYRlSgsvRSmnBSuxNpUybu4TRE2Z3u8Y4h665OLr/U7ZvXIYpLInUcdegmkIRAjLSEklKiEFzOdn96dvs3fx+p/y/F7BldcGhWT3d9DXDVkFq5tyWJnDtoWtMep3EMDH2k9e6BFB5grmZmRaz1TwT1Lkmo7rYpetTdU1vnzckJCWSmDKC+OREDCFh7DhchiEkHNViRZhMqAYzKAKln2/lHz54LakJEdTX1FFbXUNDbR1nyiooPnmSkpOFFB0/SWODe6QRQpCaEkvOqMSWrKyEvfljU19/+bXNBTt3nFhDL3teMQlpXHX7j4iITqSqpoHCoop2N5TmmpNUnfyMtoZyEjPGkZY1loioeIzmEJyONhpqqygrOkxF4QHMEUkkZF9GWFz2WXkgJzOFuNgIbK2NfPDif1Nd3rtFTxdy8doDh9f068vxAcNWQerm3PKmRH65u3sClkV/8uYSX8hx/ZQpoU6bbZyUcnx0TOgMk9l4aVubM6epsTW8r916oSgIg9G9byCU9qMCAHRH775giqKQlBRJamqsIy4+vCwtNXbf+PzMFaMyklaJ7DuLzi971fi8v0hdJCJYK6UoFUKPBmUhyDsAE7hzY33p678l1BpJm83OsZOlnUYeR1sjzTUnaa0rwWlvQtccKKoZozmM0KhUwuKyMYV0mG+toRay0pMICwvBYW/lnX/9hNrK9neWE8mLQijvasJVpmpKglSYIYVMWHPg8EP9+d59xbBVkNo5X2kEekr90RjzyZt+Dfqfm5lpMYWFxV8yPmNCVHzIeEVRR2lOVyaIeKfDFWk0qmaHyxUKmHUNpc1mV0wmg2qxGNF13WVUDbrRZNR1XbMpitIaEmJqjIgIrTKb1eLwiNBDEyZkbElJCDsosu6r6EuWnliUPzpP6Ia3QeYBJKXncdN9PwOhIKWkqqaB8ooa2mz9j00xGg2kJMeSGB/TntB649t/48jej8+WkKd11OvXFhTsGazcvuRiVZCGmE/ejOrhXpDzWJibO0IxKLuARIBFSx4le+yM80pIGhpbqG9opbGphZZWOxeefGU2mwi3hhIdFUZ0VFgnb97KshMsf/qxc7+2qJqcvOLw4aNe/2AeYvhasYRYj5RdjfVuVvtUlmHM2iNHyhaPG3cv6CsB9m398AIFEURGhBEZ4d4LkhI0XWuPJTEZ1S7ZTM7n0M6Os3Ek/HQ4KQcMY2dFzcVjQNcUGVAjFf1RX8sznFldULAKxGGAqrIT6FrPsR9CgEFVEdKJ2WTsVTmAcy4kAJpusQeM+ba/DNsRJP6zZUdq5tw2UcH1ewnn4klXSUV/NPaj5af9KtxwRMj9SPI0l5PGujNExaV0um1rbeTwno85sf9T6qtLcTrtqAYTEVHxZI2ZTu6kuUTGJndp9rzowqPrdp70fOp6LzNsFQTgrCnXJ9aqix7Z4VVx4X7HyYItfPzB09jbOrvtay4HddWl1G1azp7P3mHirBuYNu/WTucUqqrxrO8W/k9ONgiG7RQriIcRtEf01VaWtAeU7dr0NmuW/eFC5SiS8DFwDHCCO+5l96fvsGbZH9pTCVWWHD3/aLbUG3JzA+McigEwbK1YQTzHogm5WUJTOnnORsWlMCJzLId2rjs/+vJ1Xeenaw8dOtZRd0KC0JyPAI8ARnB7Drc01XbZFJQot6wpKAjI7CU9EVSQIDwBypbxY+5E8t9AendlpJRPrDl4+Gc9tbF4/JhrpWSZgJBubp8CflVtCX1+586dXWN7A5igggRp55Zx40wNaLcriG9LmHrerXdXFxzqyaTezqJxeY8JxK87rsi9IP42HBXjHEEFCdItC8aPH6Oi34yUC6WifWPN/qOH+6pz9ciRZt1ifA3EdtWlvznc9jyCBAkSJEiQIEGCBAkSJEiQIEGCBAkSJEiQIEGCBAkSJEiQIEGCBAkSJEiQIEGCBAkSJEiQIEGCDH/+P3aLNKBOEgGjAAAAAElFTkSuQmCC","");
INSERT INTO users VALUES(2,"Anonimo","","","$2y$10$Sj11x3yoH.y1bh0ul1nkiubW0FZw1YKdYpCaPmS9NDoMR1saH1YHu","C/Sierra",34657237625,3,1,"","");
INSERT INTO users VALUES(3,"Miguel","Morillo","miguelmwilliams@gmail.com","$2y$10$Sj11x3yoH.y1bh0ul1nkiubW0FZw1YKdYpCaPmS9NDoMR1saH1YHu","C/Sierra","34-657237625",2,1,"data:image/;base64,iVBORw0KGgoAAAANSUhEUgAAAGUAAABjCAYAAACCJc7SAAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AAAAqdEVYdENyZWF0aW9uIFRpbWUAdmllIDIyIG1hciAyMDE5IDE3OjAwOjMxIENFVKIglMYAABWuSURBVHic7Z1peJRFtsd/1Z1O0p2k02QnbMEQQBMYhbAjyCKyKgyLIqAMKIuizlwQRr0wKJCwKM5VYFREkEVQIlvYcRTwURCRPUg21iQEsu/pdPf73g8NLSEk6S0kM+T/PP3hXepU9fuvOlV16tQpkXY9XaYedQqK2i5APSqinpQ6iHpS6iDqSamDqCelDqKelDqIelLqIOpJqYNwqe0CVIXCggIGPvWUU2TF7tmNt7fOKbJqGnWaFDc3N8a98EKF+yaTkcyMTFJTU8nLy8NgMGAwGFCpVLi5uqLWaPDw8MDDwwONRoO3zht3d7UlvRACWa67how6Q4oQgqLCQtzc3VAqXZAlifj4eIwmI4YyAzqdDo2HB0aDgfNxcaSlpZGXm0tpaSkmkwmTJFGm11Pq4kJJSQkFBQW4ubrieouk2G3b6NSlM8GNGqFUulCQn4++TI9W642rq2tt//1yELVp+xJCUFpayqHvv+fQwYOMnzgBDw9Pvt28mW82baqxfNu1b8+48eN5rF07XFzM9bIutZxaI0UIQXJiIidPnkSn05Gfl0fsjh0kJiTYLVOj0eDq5kZeXh6yJFX/vocHAwYOZMCgQYSEhOCuVtcJcmqFFJPRSEJ8PD8ePszZM2c4f/48ZXq9QzL79e/PpKlT8PTwpKi4iLSUVKb/7a+U6cuqTatUKnn4kUfo8URPBgwaRIMGPrVKzn0lxWgwsHHDBr5av57i4mIkK2rzndBoNHTs3In2kZE0btwYP39/fHx88dJqEUKALBO7Ywe7d+4k/sIFlEolnbp0oUvXrjRv3hyAHw8fZldsLDk5OffMQ6lUMnrMGCZNnWqWWQuocVIEkF+Qz87tO1i1ciX6SlpEUMOGtGzVkqzMLBQKQWBQQ5o0acIj4eGEtmiBj68vLi4uVtXg2wRRyShLCMHZ06eZN3cu6enpqFSqCuXSaDQsWLiQdpGRKBT3dzpXo6QUFxeze+dOvlq3joyMjArPXVxciOzQgWHDhxMeEYGnlxcZN29QVmagSZMmKJTKGlMjJpOJpMQEdsXG0qRJU5QuSj7/9DMKCgos77i6utKrTx8mvPwSjYIbcb9USo2REnf2LP+YPZuMjAwkk6nC8z5PPsmrr72Gf0AAApAx12CT0YjSyhbhDNyes9weCX6yfDlbYmLKqVZvb2+GjxzBXya+BPdBpTmdFFmW2bRhAyuWLavwTOvtTb+nnmLCSy/hpdU6M1unQQhBclIi/1q+ghPHj1NW9sdAoWnTprwXtYCHQlvUaH/jVFIkk4kvV69m/bp1FUZTAQEBzIuK4pGICGdlV6OQJIkTx4+zauVKzp09a7mvUqnoP3Agf5s+HVc3txpp0U4lZdVnn/HV+vXlatdt7D6wH63Wu07MA6yFEIIyvZ6Yzd+w8pNPMRgMlmePtWvHkg+XYjSa8PT0dOr/csqworSkhPFjx7Lmiy/uScgTvXrh5aX9jyIEzKpY5erK6DFjWbdxI2EtW6JSqQA4eeIEUfPmcyM9natXLjtVnSmnz5gx197EQgiyMjN5Y9o0kpOS7vnOo489xuy5c3Fzd7c3mzoBrVbLkKefRtegARfOn6e0tJTUlBQ8PDwoLSklPz+f4EaNnJKXQ+orLy+Xt96cybmzZ+/ZCtq0bcu86Ch8ff0cKmRdw9Gff2bOO+9QUlKCRqOhz5N9kSSZzl270qt3b4c1gv2kyDLTXplKYX4BaWlplJSUWB65urkx+OkhTHvtdVR1zALrLGzcsIF/LVtmIcDLywuE4MOP/o9WrR92SLbdpKz4+GN0Oh3e3t58smIFubm5aDQa+vbrx0uTJtHAx8ehgtV1CCF4b84c9u/bV+5+2z/9iYVLljg05Le5oxdCsHf3LoIaNqRvv37s3bOH3NxcXF1dmfbGG0yfOfO/nhAwDwLGT5xY4f6Z06dZ+eknDsm2mZSUa9c4feo0fx4xgu3btnL61CkAXp48maeHDr3vdqLaROPGjQkODq5wf/vWbRz64Qe75dr0BU0mE3t27eL1v/6VxIQEtsR8S1jLlrz6+uuMHjv2P27I6ygUSiXjxr9YoSJKksTc2bMpLi62T661LwohOHfmDBFt2qB0UbIrNpYXx49nydKlPPf88w8cIWBWYU/07kN4RHiFZ0ajkeUffWSXXKtJMZlMHNi/j0fbtePHQ4eZ9vrrPDdmDD6+vnZl/N8CLy8vRj77nGVSeScO/fADly4m2yzTalJ+OXKEyA4dcXd3p3efPrjcoxAPIsytpTeRHTtUICYvL4/Y7Tts1iJWk7L12xi6Pf44AOIB6sytgRCCeQuimDvvPTQaTbln3x04wLWrV22SZ9XXjf/9dwYOGWKpCWV6Pb/9+isp167ViAn7fizDOjsHN3d3ej7RiymvvlLufk52Nls2b7YpP6tIiYs7R6/efRBCcPPGDcaNHs2ShQvx8fV1WgdvNBqIilpAm7YRtGrdkhlvTufSpYtOJ8hgMLB69ReEtQqj9cOtmDJlMgkJCU7JRwYGDBpEu/bty93fs2cPkmy9P4JVM3pZlhBCQfT8+ezdvRtJkpg9dy79+ve3ueD3QlZWFqNGjST5rk5Rp9Px9ltvM2rUKJxRt/V6PTNnvcn27dvL3RdCMGPGDKZMnopSqXQoDyEE+3bvZmFUVDlT/9bYHfj5B1glw6qWIoSCE8ePWwjR6XQ86SQfX4CFC6MrEAKQm5vL7DmzOXPmjFPy2fT1pgqEgLmzXrJkCV9+ucbhFiPLMt179MDf37/c/cSERKtlWEcKMHP6dMu69ctTpjhNraSmpnDk6JFKn+v1eiZPnuyUj/Xuu3OrfOfrb74htxLXI1ug8fCgXWRkuXtnbahYVpFy5vRpiwuOm7s7fZ7sa0MRq0ba9evcvHmzyneup1/HYKjeqa4qZGVmVutnlpWVSUZmpkP53MbLkyeVu7bF87NaUoQQbN3yreW6U6dOeHh42lC8qlFSXFxO994LSqUSpdIxX/SCwoJq33Fzc0Otds5inK+fP3969FHL9ZUrV6xOWy0p+tJSkhOT0Hh4ANCla1enjoiqIwSgW7duDudZWlpa7TtBQUH4+flX+541kGWZyVOnWq5tUYvVkpKRkYG3TkerVi1RKBRcv37d7H3oJHTr1o0ePXpU+rxBgwa8Of1Nh/N55JFwhgwZUulzIQQjR4zE3YnL1uEREfjcWsawpvLdRrWkZGZk0P3xx0lOSsY/IIBhw//sVE9Bd3c17y95n3HjxlUYjvr6+rIweiFt2rZ1OB9ZllmyeAmvvfYa2rsWoNRqNbNmzuK550Y71bCqVCotVpC786wKVSrq216DMZs3k5+fT0BgIO5qdVVJ7IK/fwDz3pvPK1Ne4cTJE5w9ewZ3tYaJEyag1Wqd1jDd3dVM/58ZjB0zjtOnTxEXF4erqyujnxtdM4ZVIWgR1gKAwMBAq5NVSYosy1y5fJmMW6Oj5KQk8nLz8PT0cqCklaNhcDCDgoMZNGjwHWVwfj6BgYH06/cU/fo5b65VVV4AzUMfsjpNlepLliTS09Mx3fIFlmX5nn5d9agcJpN5GN68uRNIEQJMkkR2VlY5PeuoGeLBg/nbtQgLszpFpaRIJglJkigu+WNJ09XNzamjkwcBtze5Nm7SxOo0lZJSUFAAsozJ+Mc2Bh8fH9zc3Bwo4oMHNzd3WrduTWBQkNVpKiUlJeUaUH7XbNNmTWtk9PXfClmW0Xp7M3HSyzZ5+VT65tGffgbKLzi1bv1wfUuxEaEtQunctZtN859KSfnll18oKCgo17H7B1i3HlCPO2G7eahSUowGA3HnzpZzBjAaDbW2Y/Y/EUIIm3dAQxWk+Pr6cj7ufDkHbWuMevW4E7Jd6zOVktI0JIQbN9JRKv94JT8vH9mGteYHHZIkc/PGDZvTVUpKeEQEXlotwcGNLCosKyvLMruvR/XIzs5CssNOVCkpYWFhNG7UmNAWLWh7a7EmLTXFqrAa9TDj+LFjdkVIqpSUwIYNcVerKSwspGfPngBcunSZwsJC+0v5gGFLzLd2WUAqJUWlUtEwuCHFRUU0bdbMEnjg2rWr9SMwK1BcVERubi5ab9s3D1VhkBSEhDTHW6dDpVLh7u5OWMswThz/zSn2dKPRyKZNGzl58oTTvRXthTPLceSnn9Co1Wg0HjanrXzuL8vodDoaNW5Eeno6nbt2xVunI3b7drvG3ndj1qyZ/P2tvzPsz8P4/fffHZbnKD7/fCVP9X+K1NQUh2UJARu/+goPT897euNXh0pJkTEHtGkYHMyxX36hV+/e/B53nrKyMi5fvuRImQEYPHiwRQ1Oe30aklR7o7ozZ07z/gfvcyH+AjExMeDggnfGzQyyMjPp2KmTXcvLVVvJBHh5afH21hLRpg05OTmUlJTwxcrP7S2vBb169WZA/wEAJCUlsWDBAodl2gMhBEePHrX4tZWUlDiknYUQHD1yhJycHMv6vK2okhSFQomhrIyXJk/Bz9+PsJYtMZlMHD50iCInjMLmzPkHDRo0AGDturV8990Bh2XaCqPBwLm4c5YaHR4e7tC+Tb1eT25uDmq1muYPWb/aeCeqXg6WZUySCbVajUKhZPiIEYB5T9+6tV86HGWpYVAQ896bh9pdjcFgYN78eSQnJ93Xjj8tLY1jx44B4O/vT3h4uEMeLakpKUgmiSd697ab3GpTeXp6UVRYiCzLtO/QgcCgQBQKBfv37qMgv3qvw6ogAwMGDLzlVW/2IpwxYwZXr11zSK4tOPDdAdLT0wEIDQ2ladNmDslLiL/AzRs3GPXss3bLqJYUhULBlSuXAfDz9+d/ZryJr58fWZmZ7Nu71+E5i1KpZM6cf9C6VWsATp46yejnnyM+/oJDcq1Bbm4Oy1cst1yPGjnKEhLXHtxIv47RaKKouJgQO1UXWOngHRzcCEmSUCgURLRtw99ueeDHbt/uFC91pVLJzp27iGxvjteYkpLCoMGDOHTooMOyK4MQMHPmTLKysgCzs9zgwZV7UFqDIz//jIeHB126dnWoX7IqZQMfH/R6PUIIPD296N6jBx06diQ5KYkd27bZnfmdcHFxYe3adYwcMRIXFxeMRiPTXpvGZys/s3s/elVYs2YN+w/sB8wjphXL/+VwJO+wsJZs27qFTl06O9QvWUWKLMto1GpMJpMl3uLbs/8XIQQb1q3jyuXLdhfgTmg0GubPn8+LL7wImJ03Fi1ayAsvjuPHw4eckgdATMxm5i+Yb7l+YdwLdO/e3SGZifHxLIqOpmOnTg4fdGBTwBxJksjJzsbP35/069f5euNGNn/9NVpvb3Y6oX+xFEoI1q9fR1RUFEXFRZb7ERERLF+2nGbNQuySK0kSH364lGXLl1kqV8cOHVmz5kvUDjiEGA0Gxo0ejSTLfLJypcOxaWxSfAqFgtzcHMspDL379CEgIID8vDw+WrrUoYLcCVmWGTNmLPv27mPkyJGWD3bu3Dme6PUEEyZO4OjRI+Tm5mIymaodmhcXF/Prr8cYPGQQHy/72KJaIiMj+eijjxwiBCAxMYGc3ByeHe0cn2SbQ0sZjUZ+OXqEbt0fJy0lhZ2xsaxfuxY3NzfmR0fZ7LlRHSTJxKlTp3jrrb8Tf9duqIYNGxLZPpLQFqE0D2lOYGCgJZ5jUVERycnJJCcnc+zYMeLOx5UrV58+ffnnh/80x+lyAGV6PXPnzCE3J4fln37qFG1hV7yvwwcP0qFTJ9RqNVcuX2LW9BmkpqYSGBTEik8/JcAGD3OrCyoEBw/+wNtvv01qWuo9nysUCstHkWX5nquk7u7uLJi/gBHDRzi8pUMAa1Z/wfov17Jt1y48PGy3CN9Trr1B2HbFxjL46acBmR3btrM4OhqADh07Er14cQ3GjJRJTExk69atJCQmkBCfQNr1NIxG4z3fViqVBAQEEP5IOD179mTo0GEOtw4wV4J/HzjAB4sXM39hNO3aRzpNQ9hNSmlJCUII88eXZWa/8w4Hv/8egN59+zLn3Xdr1BlcYFalefn5ZGdnkZqayo2bNykqLKSsrAwvLy98fHxo3LgxQYFB6Bo0cGhieHfe//7uO6IXLGDylCmMcGD2fk/5jgT2vB0OHczbx8Y8+yzX09IA897IRR98cF9XKSvmJTt9f4sQsGfXbt5ftIjOXbowLzra6f/Rocg3d/5flUrFkqVL8fA07xw+euQI8+fOpbCg4L4RI8vyXT+nZ8Cu2J0sWWje8vdeVFSN/DenhiNq1KgRI58dhZfWC1mW2b9vH1MnvcyRn37C0YWj2sLtj15SUsKqzz5jcXQ0/QcOZPHSpTUWmtHpBxCUFBdz4rffWL1qFfEXzEZFV1dXuvfowYyZM+vswQP3QmFBAXq9npzsbJYsWsT5uDieHjqUv06fbtcyr7WokaM6srOyMBqNbImJYUtMjCVmsY+vL3PmzqV9hw7OztKpEEKQlpbK+XNxXL50kfVr1yFJEv0HDGDWO2+jUNTsbrYaOz+luLgIhVAQd+4cH77/frmIC3369mXipJdp2iykzsWelCSJxIR4Tp08ya7YnVy6eBEvrZYpr7zCkGeeuT+xyGrypCEBIAQF+flEzZvHj4cPW555eHrwlwkT6de/v1PjhtkLIQS/nz/Ppg0bSE5K4urVq0iSRLOQEOa8+y6tWre+fwft3K+D0oQQ7Nyxgw8WL64QfaFL1668u2CBwzYouyDLZGdn8/NPP7EoKspyWwhBWMsw/vnxsvveD9Y4KbIsUVxcbNl7n5iQwJerV/PjoUPlzCCenp5MffVVunbvjq+fH0KIGjtOVgiByWTCYDBgMhpJSUnhnVmzLMvCTZo0YdTo0TwzdGitxMus+dPrhODypUuoVC4EBAZhMBgoLCigqKiQzV9/zb49e9Hr9Wi1Wqa98ToPhYbi4uKCp6cnAQGByMjmliXjNNONEAKjwUBmZia7YmPZtmULOTk56HQ6hg0fzjPDhuHn719rKvW+qi9k+daBaLdP/DP3N6tXreKbTZvw9PKiXbt2PD92LK1at7aE2b3dtVpb0Dtb2J2TSb2+lJzsHM6eOU3M5s1k3Mzg0UcfZcCgQbRs1QofX986cdhzrZ4dfCckk4mMjJu4u6u5cuUy/z7wHXp9KT4+vvj5+xES0pzgRsF4e+tQubqivGURljHbwPbv3cvKTz4hOzu7wkdVKBT4BwTQtGlTWj/8MJ06d+ah0NA6O2eq9QOdK6uVQggMBgMF+fncvHGDa9euceniRVJSrqEv1eOiUqHVainT67l69QoXky9a/Ah8fX0JaR7CQ6EtaBEWRkhICIFBQXh7e+OiUtV6S6gOtd5SJEniyuXLNG3WrEqrsnl+YLZn3VZHRqMRQ1mZ2eFcmO1vKpWrZV3l9pC8rpNwN2qdlHpURH189DqIelLqIOpJqYOoJ6UOop6UOoh6Uuog6kmpg/h/xTvXulS4MWgAAAAASUVORK5CYII=","");
INSERT INTO users VALUES(30,"Miguel","Morillo","miguelmwilliams@hotmail.com","$2y$10$tIkF0yHjum6NioSB5pzPreiQ/EkYrQeq0C2LfXCNzRb1zd9iHXk1e","C/Sierra","34-657237625",2,1,"data:image/;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABbCAYAAABnEjtVAAAABHNCSVQICAgIfAhkiAAAABl0RVh0U29mdHdhcmUAZ25vbWUtc2NyZWVuc2hvdO8Dvz4AAAAqdEVYdENyZWF0aW9uIFRpbWUAdmllIDIyIG1hciAyMDE5IDE3OjAwOjM4IENFVK8w9rcAABXhSURBVHic7Z13eJRV9sc/d0qmZNIhgfRQglIEITT5UaVIEanurjRRQPCRBSmCIAsrIIJYWFAQRECKsOJKiYBYKCKrNFdCSaSZBiGQhGQyM5n6/v4YCMSQZJKZkCh8n4fnIfe9954z7/e+t51z7hWXr2RIPEC1gKyqFXiA23hARjXCAzKqER6QUY3wgIxqBEVVK1Aa8vV6enbr5pG64r/ag5+fv0fqqixUazJUKhXDRowolm6z2biWmcnl9HRyc3Ox2mzYrFYUCgVeKhVajQattzfe3t5ovb3x8/NDrdYUlhdCIEnVb0Zfrcgw5Oej1qiRyxU47HYSE89it9uwWK34+fmj1Wqx22z8mpRExpUr3MjNxWQ0Yrfbsdvt2Gw25z+rlQKzGYPBgFqlIi83l/jt22nZujWhYaEoFEoMhnwsZgs6nQ4vlRfVgRtRlYs+IQQmk4kvd+7gl5//x6gXxqBQKNi4fgM7tm2rNLmNmzRhxMiRtGzdGoVCUW2+kiojQwjB6VOnSDx7ltq1a3M1I4PPt27lt0uXKlynTqdDo9WSnZWF3W4vM79Go6F7jx70GziAyKhoVCpVlRJTJWRYrVYunD/PsSNHOHrkCKcSErBYLBWuTyaT0efJJxkyfBje3joM+flcunSJWTNnYLVYyywvhKBxkyZ07d6dzl26EBgUVCWk3FMyzAUF/HvzZnbv+pJr165TYDKVq3xIrVq079iBRo0aExwSQmBgIH5+fnjrdMjlciRJ4tuv97Jz+w4yMjIwmYx07vI4zR59lLDwcARw7OhRdsXH89tvvxV74TKZDH9/f/r07ctzo0cjl8s9+OvLRqWTIYTAkJ/Pls2fsnb1xzgcjrvmCQkJITQsDL1ejxAC/wB/IiIiadO2LbENGhAQGIgQwmN6nTl9ivn/fJ20tDTUajUmk6kIOQqFgoWLF9OydWuPyi0NlUqG0WDg6717Wb1qFTnZ2cWeq9VqWrdtQ9du3WnyyCP4BwSQceUKkiQ5W3IlvgS73c7phAQO7N9PQGAAapWaTRs3ci0zszCPUqmka/duPDdqFLVDwyq966oUMoQQnDh2jPeXLuX8uXPFBlMhBH8bMoRBTz9NjZo1kclkSJKEEAKHw4FMCO51jy0EGAwGNq3fwKcbNxYZw2rXrs1T/fvz1yFDKrXrqhQyVq9axdrVq4ulq9RqunXvxsRJk1Gp1Z4W6xEIIUhJTmbGtGmkpaZis9kKnwUHB/PGooU0eOjhypHtSTIcdjvr1qxh/bp1WK1FZzHhERG8OnMmjzRr5ilxlQpJkvjh++/5dONGTiUkFI51MpmMXr17M2nqVLw8PBX2HBmSxCdr17JuzZq7TlN37NpVZVPGikIIgcloZM/u3SxbsgSz2Vz4rEOnjsz8x2wEoPX29sjv8ggZRoOBUc8+S3p6+l1nS506d2LugjfdFVNlEEB2VhZjx4zhcnp6YXqnzp15bvQoZEJGdEyM2+OcfPKUKXPcUTInO5sXx4whJSXlrq3joYcf5p/z5lfbMcJVaLRaBj39ND4+PiQmJmIuKCAtLQ1fXz/y8nIpMBdQu3aoWzLcIiP3xg1mv/YaiWfP3vV502bNmLdgAYGBgRUVUa0ghKBR48bUqVOHH77/HrPZTFJSEsE1a3L06BG8td5EREZWvP6KdlOSw8GkCROw2axcuHARfV5e4TOZTMaESZN4sm9flF5eFVauukIIQfyO7bw5/w3AuUD09fVFkiTeevddGjz0UIXqrZClTwjBgvnzebR5c558qh+3lmYqlYqOnTux+5uvGTBo0J+SCHDOtHo/2Zeu3bsDTvtKdnY2OTk5LF64kHy9vkL1lpsMIQR79+whNjaWJ3r1Yld8PHl5eahUKia8/DKvz38Drda7Qsr80fD86FHF0hLPnmXNXdZYrqDcZKSnpXLh/HkGDB7Ejm3bOHH8OACTX3mFJ/v1Qya7f8zqoWHhREZFFUv/z9atHNy/v9z1levN2e12/r15C8+NHs35X8+xft06QkJCGDN2LD179y638D865HI5fxsypFi6zWZj5vTpmMq5K+0yGULAmVOnaNmqFUIItn72GeMnTuSDlSsZPnJkuYT+WSBJEp27dKFho0Z3ff7e4sXlqs9lMmw2O/v37aNFXBwJJ39h8tSpDBg0iJrBwX+oVbWnofPxYejw4ahUqmLPDv/wA5cuXnS5LpfJOPLjjzRp+ghqjYZHm7dApVbfV+NDSZAkifYdOlA/NrbYs9zcXOJ37HC5sbr8Nj/duJF2/9ceoNLtDLe2VCpTjsNux2G3A5L7coRg6QcfMOMfs9BqtYXJkiSxKz6e9LQ0l6pxaQWelJhIREQEdevVA8BkMvLbpYsoFArUHtrmSElJ5u2332bmazPYsHEDp0+fRqlQEBkZ6bEvUADXr19jxYrlTJ46hVUfreJkQgIgERMT45YcmVxObGwsarWaY0ePFn4NFosFJIm2jz1Wtn6urMB3x8fTs08fJEni/LlfWfreEq5cvswnmzahuaMlVBQnT/7CmDFjyLiaUSRdq9UyaOAg5syZg0zmvlHnxo0bDBs2lFOnTxUzsXZo34GlS5fh7e3eGslkNPLqK69w/NixwjQfHx/iv/qqTLJdagq9+vQB4B8zZvDc8BH8fOIEz48Z4xEiJMnBkn8tKUYEgNFo5JP1n3Dw4EG35QB8+OEKEk4lFOvDbTYb3+37jrcWL3K7y9J6e9O9R48iL16v15N1/XqZZV0iQwJ++fln9u/bB0CNmjXp0bNnxbT9HVJSUjh9+nSpeaa/Oh13Rw+73caqj1aVmufQoUNkZZX90kqDJEl07NKFkFq1iqRfOH+uzLKudZKSxBvz5hX+OfK55zw2uF69epWsrKxS82RkZGB2w68K4Pr1rCIm1LshJzuHrOul6+IKdDodHTt1KpJ2+lTpDQ5cJOPkyV+KGFW6PdGjfNqVAqPRWMxE+3sIIfByc9NRr88rM4/OR4evn59bcsD5dTwzdGiRtKTExDLLlUmGEIKtW/5d+HeHTp3QaNwfK25Bo9Gg0WhKzTNgwAC35ajVmjIJjYiIICgoyG1ZAIGBgTzc8LbjgiuLvzLJMJvNJCcnF7aYJk2a4Mnpf1xcS3qVMv7UrVuXSS9PdltOREQEEydMLDXPxAkTUSo945gvAS/dIS83N7fMMmWScS0zE/+AAOrXrw/g3Pzy4O6HXC5n4cK3mDljZrFxqFatWrzz9ruEh4e7LUeSJMaNG8d77y0pZnlUq9Ws/mg1LVu28mhoQOPGjQkODga4q2/A71HmOuPEsWPo83KZ+8/XCQwKYtXHH+PnXzkRQCaTkTNnznD27FnUKhX9BwysFKexggITSUm/kvRrIiovFb169UapVHpcjgAWvfkmO7Zto0aNGnwRH19q/lK/SSEEVquFlSs+xGw2o1arkMkrbz9Ko9HSokUcLVrEVZoMcI4fTZs2pWnTppUqByGoW68u4FwOlIVS36wkSaQkp3D58mUA0tPSK2xSvF9xi4SYmJgy85ZOhsPBlSuXC31lLRYLBaYCD6h4H+HmIBDtDhlCCOwOB9lZ2UW2D+SKahUGWO0h3WQjtkGDMvOWSMatrWzjHaZDlUrlsV3a+wWqm2ubqOjoMvOWSIZerwdJKrKF4B8QgJfqz+l+U1lQeqmIbdDAvQE8PS3V+Z87uqiIyAg06tJXyw9wG5IkERDgz8hRz7tkKykxx0///RHpd1awhg0b/eF9Zu81omPq0K7d/7lkei2RjGNHj5CXm1dkXVGjRg23t7LvN8hkMoSLFsQSc+XnGziVkICX8vYY4XA48OjG1J8ct8LiXEWJZISEBJOUmIjS6/Y2QXmdsh5AIvfGDZdzl0hGVHQ01zIzi9ie8/LykCTXmb7f4XBIZN4RPVsWSiSjYaNGaLRaQkNDCzfRXD0G4gGcyM7OLtf7KpGM+rENiIyKon5sLI0aNwYgPS0Ni9k98+f9hGM//VS46HMFJY8ZtULQqNXo9Xl06fo4AMnJyc7F4AOUCSEEX8bHoy7DinknSiRDqfSidlgoJqOJyKgohBBYLBbS0lLv2fENf2Tk5+dz9epVfHx9XC5T6kZhVFQ0vn5+KBVKNBoNsbGx/Hz8BB41h/1J8ePhw2i1mnIFDpW8BStJ+Pn7ExYWxuXL6bRq04YCk5Fvv/mGkc8/j+wenTgjhMBms2EymcjPz8doNGI0GpDLFWg0GrRaLd7eWlQqNUqlotq0k+1ffIG/fwBKpdJlx+cSyZBwuj2GhoXxn8+38ni3rrz39jsUmEwkJycTU6eOp/S+K4RwOrjt27eP48ePk5iUyKVLl4oF/Ot0OiIjI4mKiqZJ48bExcXxyCNNq3R3+fq1a6SmpNB/4IByhUuUaZzw9fXFy0tFw4aNyL7pbLZyxQoWLFpUcW3LwI2cHJ59biRJSYkUFBSU+oPy8/M5c+YMZ86cYffuXSiVSrRaLZ07d2HWa68RFFSj0vS8G4QQ/HDoEFlZWbRr36FcZUslQy6XYzIaeXH8eHQ6HbGxsSQlJXHo4EGMBgNaN52Ei0GS2Pv1XqZNn0ZOTk5hskqlokFsLGHh4QQFBqHRaFBr1Ojz9Ojz9Vy7do309HSSk5OxWq3k5uaybdsX7Nmzm+HDhzNu7DgCAwLvyUk95oICsrKu4+Pj45Kp9U6U6R2Sl5uLr5+fc6q2cycLbrp5DhsxghdefNGDUUsSKz5cwdKlSzEYDAB4e3szeNBgBgwYQN26ddFqtchk8uKOy1Yr+nw9ly5d4uDBg2zespmMjNuO1ME1azJ+/N8ZOHBgpUbiCuDSpYsc3H+AzMyrTJk2vXzlyyJDcjgwGAzofHzIvHqVZ4cNQ5+XR0hICGs3bEDn4/rUrUQlBMyePZv1G9YXrlh1Oh1f/OcL6tcvHhFUFszmAvbs2cOMmTMKiQVo/mhz5s2bT6NGjSot9G33l1+S8MsvDPrL09SpW69cZct275TJSElOBqBGzRpMnzGDsPBwMjMz2bNrl9trDiEEGzduYu26tdjtdmQyGe3atePE8Z8rRASASqXmqaf68b+ff2HypEmFTmsnfj5B/wH9+OqrPW7pXBIyr17FXFCA2Wwmpk7dcpd3KXLJy8sLLy8lMpmcwKBAoqOj+WbvXrKzs+nYuVOR05TLi4wrV5g5ayY5OTkIIejT50neX/bBXQMWywu5XE6bNm3o0f0Jcm7kkJSUhN1uZ2f8ThQKBS1axHk0LvH4sWPY7HaioqOpd9MDs1z6ukKGRqPBXFCAUqlE6eVFRGQkJ0+e5PSpU3jrvGnW7NGK6I4kSSxf/gHffPsNkiTRpHET3nnnXXx9fStUX0nw9/ene7fuhIbW5sCBAzgcDg7/9zD6vDwee6wtcrlnPF7y9Xo+3/oZQ4bdPfq1LLgWLCNJqDUa7A4HDocDIQSzZs9GkiRWrfiQ9DvCBcqD7KwsPl7jPNFTJpPx7IgR1HTBcF8RyOVy/vbXZ/h49ceFXu9r1q7h3Xff9cj4ceXKZSaMH09cXEv8KxhWUK5vVOBcA9waJwb/5WkAprw8sdxb6wLY+vlWCgqcTnHh4eH06+++639pkCSJDh06sPqjjwu/vhUfrmDLls1umZOtVisvj/87QUFB9Ozdu8JT6HKRIZPLyM29gc1qxWw207FTZ0Jq1SI1JZVlS5aUS7BDcrBy1crCv6dOmXJPDvWVJGjXrh2rP1pdeA76rH/M4oAbcYPnfk3iRk4OQ4YOJdCN+I5ykSFJEBkVzX8PHyYsPIzAoEB69u4FwI5t2zh86JDLs6sLFy4Who85B+6+5VS94pAkiVYtW7FqpTPGz2q1Mu7FsUXWJq7CYjazfu06goOD6du/v1tdXrmnEs7W5MBkNBERGcXjXbsRHhGOxWJh2b/+RcaVKy7Vs2XL5sL/9+/f/55vy0tA506dGfvCWORyOQaDgVdnTC+XAwHA51s/4+yZMyxbscJtnSo0r2vfsROHf/gBISCmTh1G3DzIJTUlhcWLFrl0xnn65duD/uBBT1dEDfchBOPGjaPpI87QgOPHj3P06BGXGsatc7c+3biJ2XNf90jMSoUn2e3at8doMCJJEk/06s1j7doBzn38ObNmIZXRwia/PJmQkBB69+pFXFyLiqrhNvz9Axg9ajRKpZK8vLzCqW9pcB6Lt4O3Fy1i2IgRPNq8RdUfpSrEbTuTzWbjLwMHknn1KgAt4uJ4b+nSUv2snC1QqhY2iEVvLWL37l0s/2A5D5VyorMQgj27vmThGwto07Yt8xcu9FgX69ETn8+fO8dLY8diMBicd1r07cvosS8QEBD4pzgGSZIk9uzaxXvvvEPLVq2YM3cuCg+GSLh1lOrv4evnh81mIykpCavFQlJiIkd+/InAwECXXOKrI261+oICEyuXL2f1qlX0eOIJps2c6fE4QI8fTG8yGjmVkMAb8+Zx/do1ABRKJU2aNGHh4sUeOW/kXsFoNGDIN2Aw5DN96iukp6XRs1cvXpkxw6NfxC1Uyi0B2VlZWK1Wvvh8Kzu37yDv5pm3tUNDmfHaazRt1sxlZ+CqgBCCnOxs9u/bx42cHDZt2IDZbKb/wIG8NGFCpRABlXiZicloRAhB4tmzLFqwgNRUZ7yHQqGga/fu/PWZZ6hXv361G0scDgcXzp3jwP79HDt2lNMJp9BqtUyaMoXuPXtW6nqoUm+WEQA3r/l5ddo0TtxxBpPOx4dnhg7hqX798fP3r3JShICkxCQ+WvkhKckpXLl8GYfDQVhYGHMXLCC2QYM/5s0ydxUkBDu3b2f5smXo9foiP6xn715MmjIVjVZbJaTk5t7gwHf7eGvhwsI0mUzGo82bM2fuXPwDAu6JHpVOhiRJ5Ov1N88ekQrPw/3u22+LvHg/Pz9efOklWrRsSXBICEKISrv285YvlsPhwGIxk5Kcwtw5c0hNSQGc4XJDhw/niZ697pl/GNyj28hSkpOdn3x4GBazBaPRgNFoZNP6Dez96iusVis6Hx8mT51KTJ062Gw2dDodtUNDkSQHVosVSZI8NhMTQmC327mWmcmObdvYuX07OTk56HQ6Bg4ezMDBg6vk4pUquTTx1spdCIE+L48V77/P9m3bUCqVtG7bhudHjaZuvXpuzbjETSESgCThkCSsFgs3cnJISDjJxvXrSU9Lp2mzZjzVvx8PNWxIjRqVY9hyWeeqvNv1Tthv3mSs0qhJTUnh+wMHsdvt+Pj6EhQYSERkJLVDa+Pr64fSywu5XF64nWK12vhm7142fPKJ0yngjut4wNn/1wwOJqZOHR5++GGax7UgJqYOfn5+1Sosrsov2i1svXd5ZrVa0eflcS0zk9TUFC5euEB6WjoWqxWFQo6vry9ms4W01FQuXryI8aZbTmBQEDExMdStV4/69esTHRNDcEiI04m7HL6v9xpV/mU4HA6Sf/uNyKioUi19d+t2bDYrFovFaZdHFDpMyGQy5wTAWbDavvzfo8rJeIDbqL57EvchHpBRjfD/yN8C91lZoLUAAAAASUVORK5CYII=","");


