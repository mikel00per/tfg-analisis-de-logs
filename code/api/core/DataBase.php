<?php

namespace Logger;

use Dotenv\Dotenv;
use Exception;
use PDO;

class DataBase
{
    private static $instance;

    private static function connect(): PDO
    {
        $env = Environment::getInstance();

        return new PDO($env->getString('DB_DRIVER').
                ':host='.$env->getString('DB_HOST').':'.$env->getString('DB_PORT').
                ';dbname='.$env->getString('DB_NAME'),
            $env->getString('DB_USER'),
            $env->getString('DB_PASSWORD')
        );
    }

    public static function getInstance(): PDO
    {
        if (self::$instance === null) {
            self::$instance = self::connect();
        }
        return self::$instance;
    }

    public static function import(PDO $pdo, $sqlFile, $tablePrefix = null, $InFilePath = null) : bool
    {
        try {
            // Enable LOAD LOCAL INFILE
            $pdo->setAttribute(\PDO::MYSQL_ATTR_LOCAL_INFILE, true);
            $sth = $pdo->query('SET foreign_key_checks = 0');
            $sth->execute();
            $sth = $pdo->query('SET UNIQUE_CHECKS=0');
            $sth->execute();
            $sth = $pdo->query('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
            $sth->execute();
            $sth = $pdo->query('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
            $sth->execute();
            $sth = $pdo->query("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
            $sth->execute();

            $errorDetect = false;
            $tmpLine = '';
            $lines = file($sqlFile);

            foreach ($lines as $line) {
                if (substr($line, 0, 2) == '--' || trim($line) == '') {
                    continue;
                }

                $line = str_replace(['<<prefix>>', '<<InFilePath>>'], [$tablePrefix, $InFilePath], $line);

                // Add this line to the current segment
                $tmpLine .= $line;

                if (substr(trim($line), -1, 1) == ';') {
                    try {
                        // Perform the Query
                        $pdo->exec($tmpLine);

                    } catch (\PDOException $e) {
                        $logger = new LoggerModel(self::getInstance());
                        $logger->error('Error la query a importar es muy grande');
                        $errorDetect = true;
                    }

                    $sth = $pdo->query('SET foreign_key_checks = 1');
                    $sth->execute();
                    $sth = $pdo->query('SET UNIQUE_CHECKS=1');
                    $sth->execute();
                    $sth = $pdo->query('SET SQL_MODE=@OLD_SQL_MODE');
                    $sth->execute();
                    $sth = $pdo->query('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
                    $sth->execute();
                    $sth = $pdo->query('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
                    $sth->execute();
//                    $logger = new LoggerModel($pdo);
//                    $logger->activity('Importada la bd');

                    // Reset temp variable to empty
                    $tmpLine = '';
                }
            }

            // Check if error is detected
            if ($errorDetect) {
                return false;
            }

        } catch (\Exception $e) {
            $logger = new LoggerModel(self::getInstance());
            $logger->error($e->getMessage());
            return false;
        }

        return true;
    }

    public static function export(PDO $pdo, $tables = '*', $filePath = '/') : bool
    {
        try {
            if ($tables == '*') {
                $tables = [];
                $query = $pdo->query('SHOW TABLES');
                while ($row = $query->fetch()) {
                    $tables[] = $row[0];
                }
            } else {
                $tables = is_array($tables) ? $tables : explode(',', $tables);
            }

            if (empty($tables)) {
                return false;
            }

            $out = '';

            // Loop through the tables
            foreach ($tables as $table) {
                $query = $pdo->query('SELECT * FROM ' . $table);
                $numColumns = $query->columnCount();

                $out .= 'DROP TABLE ' . $table . ';' . "\n\n";

                $query2 = $pdo->query('SHOW CREATE TABLE ' . $table);
                $row2 = $query2->fetch();
                $out .= $row2[1] . ';' . "\n\n";

                // Add INSERT INTO statements
                for ($i = 0; $i < $numColumns; $i++) {
                    while ($row = $query->fetch()) {
                        $out .= "INSERT INTO $table VALUES(";
                        for ($j = 0; $j < $numColumns; $j++) {
                            $row[$j] = addslashes($row[$j]);
                            $row[$j] = preg_replace("/\n/us", "\\n", $row[$j]);
                            if (isset($row[$j])) {
                                if (is_numeric($row[$j])){
                                    $out .= '' . $row[$j] . '';
                                }else{
                                    $out .= '"' . $row[$j] . '"';
                                }
                            } else {
                                $out .= '""';
                            }
                            if ($j < ($numColumns - 1)) {
                                $out .= ',';
                            }
                        }
                        $out .= ');' . "\n";
                    }
                }
                $out .= "\n\n\n";
            }

            // Save file
            file_put_contents($filePath, $out);

        } catch (Exception $e) {
            echo "Error";
            $logger = new LoggerModel(self::getInstance());
            $logger->error($e->getMessage());
            return false;
        }

        return true;
    }

    public static function delete(PDO $db) : bool{
        $sth = $db->query('SHOW TABLES');
        $tables = $sth->fetchAll(PDO::FETCH_COLUMN);

        $sth = $db->query('SET foreign_key_checks = 0');
        $sth->execute();

        $errors = false;
        foreach($tables as $table){
            $sth = $db->prepare("DROP TABLE $table");
            if (!$sth->execute()){
                $errors = true;
            }
        }


        $sth = $db->query('SET foreign_key_checks = 1');
        $sth->execute();


        return $errors;
    }
}