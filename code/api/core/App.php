<?php

namespace Logger;

use Dotenv\Dotenv;
use JsonException;
use PhilKra\Agent;

class App
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var Environment
     */
    private $environment;

    /**
     * App constructor.
     * @throws JsonException
     */
    public function __construct()
    {
        $routes = $this->loadRoutes();
        $request = $this->loadRequest();
        $response = $this->initResponse();

        $this->router = new Router($request, $response, $routes);
    }

    public function handleRequest(): void
    {
        $this->router->handle();
    }

    /**
     * @return false|string
     */
    private function loadRoutes()
    {
        return file_get_contents(__DIR__ . '/../routes/api.json');
    }

    /**
     * @return null[]|string[]
     */
    private function loadEnvironment()
    {
        $dotenv = Dotenv::createImmutable(__DIR__ .'/../');
        return $dotenv->load();
    }

    private function loadRequest(): Request
    {
        return new Request();
    }

    private function initResponse(): Response
    {
        return new Response();
    }
}