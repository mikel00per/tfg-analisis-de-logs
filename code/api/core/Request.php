<?php

namespace Logger;

class Request
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    private $domain;
    private $path;
    private $method;
    private $params;
    private $cookies;
    private $session;

    private $productionPath = '/~williams1920/proyecto';

    public function __construct() {
        $this->domain = $_SERVER['HTTP_HOST'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->path = str_replace($this->productionPath,"", $_SERVER['REQUEST_URI']);

        $this->session = new Session();
        $this->params = new Parameters();
        $this->cookies = new Cookies();
    }

    public function getUrl(): string
    {
        return $this->domain . $this->path;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getPath(): string
    {
        return substr_count($this->path, '?auto=') >= 1 ?
            str_replace('?auto=', '', $this->path) :
            $this->path;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function isPost(): bool {
        return $this->method === self::POST;
    }

    public function isGet(): bool {
        return $this->method === self::GET;
    }

    public function isPut(): bool {
        return $this->method === self::PUT;
    }

    public function isDelete(): bool {
        return $this->method === self::DELETE;
    }

    public function params(): Parameters
    {
        return $this->params;
    }

    public function cCookies(): Cookies
    {
        return $this->cookies;
    }

    public function clearCookies(){
        setcookie('user', null, -1);
        setcookie('user_name', null, -1);

        $this->cookies = new Cookies($_COOKIE);
    }

    public function redirect(string $url_relative)
    {
        header("Location: " . $_SERVER['PHP_SELF '] . $url_relative);
        exit();
    }

    /**
     * @return Session
     */
    public function session(): Session
    {
        return $this->session;
    }
}