<?php

namespace Logger;

class Session extends FilteredMap
{
    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct($_SESSION ?? []);
    }
}