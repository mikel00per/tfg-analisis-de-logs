<?php

namespace Logger;

use JsonException;

class Response
{
    protected $STATUS_HEADERS = [
        200 => 'Ok',
        201 => 'Created',
        202 => 'Accept',
        204 => 'No Content',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        307 => 'Temporally Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        412 => 'Precondition Failed',
        415 => 'Unsupported Media Type',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
    ];

    public function __construct()
    {
    }

    /**
     * @throws JsonException
     */
    private function jsonResponse(int $code, array $data = null, string $message = null): void
    {
        header_remove();

        ob_start();

        http_response_code($code);
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');
        header('Connection: close');
        header('Status: '. $this->STATUS_HEADERS[$code]);

        echo json_encode(array_merge(
            $data ?? [],
            [
                'status' => $code < 300, // success or not?
                'message' => $message ?? $this->STATUS_HEADERS[$code]
            ]
        ), JSON_THROW_ON_ERROR);
        $size = ob_get_length();

        header('Content-Length: '.$size);
        // flush all output
        ob_end_flush();
        ob_flush();
        flush();
        //die();
    }

    /**
     * @throws JsonException
     */
    public function ok(array $data = null, string $message = null): void
    {
        $this->jsonResponse(200, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function created(array $data = null, string $message = null): void
    {
        $this->jsonResponse(201, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function accept(array $data = null, string $message = null): void
    {
        $this->jsonResponse(202, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function noContent(array $data = null, string $message = null): void
    {
        $this->jsonResponse(204, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function  movedPermanently(array $data, string $message = null): void
    {
        $this->jsonResponse(301, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function found(array $data = null, string $message = null): void
    {
        $this->jsonResponse(302, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function seeOther(array $data = null, string $message = null): void
    {
        $this->jsonResponse(303, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function notModified(array $data = null, string $message = null): void
    {
        $this->jsonResponse(304, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function badRequest(array $data = null, string $message = null): void
    {
        $this->jsonResponse(400, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function unauthorized(array $data = null, string $message = null): void
    {
        $this->jsonResponse(401, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function forbidden(array $data = null, string $message = null): void
    {
        $this->jsonResponse(403, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function notFound(array $data = null, string $message = null): void
    {
        $this->jsonResponse(404, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function methodNotAllowed(array $data = null, string $message = null): void
    {
        $this->jsonResponse(405, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function notAcceptable(array $data = null, string $message = null): void
    {
        $this->jsonResponse(406, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function preconditionFailed(array $data = null, string $message = null): void
    {
        $this->jsonResponse(412, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function unsupportedMediaType(array $data = null, string $message = null): void
    {
        $this->jsonResponse(415, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function internalServerError(array $data = null, string $message = null): void
    {
        $this->jsonResponse(500, $data, $message);
    }

    /**
     * @throws JsonException
     */
    public function notImplemented(array $data = null, string $message = null): void
    {
        $this->jsonResponse(501, $data, $message);
    }
}