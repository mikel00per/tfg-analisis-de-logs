<?php

namespace Logger;

use JsonException;

class Parameters extends FilteredMap
{
    /**
     * Parameters constructor.
     * @throws JsonException
     */
    public function __construct()
    {
        $_PUT =  $_SERVER['REQUEST_METHOD'] === 'PUT' ?
            json_decode(file_get_contents('php://input'), true,512, JSON_THROW_ON_ERROR) :
            [];

        $_POST = $_SERVER['REQUEST_METHOD'] === 'POST' ?
            json_decode(file_get_contents("php://input"), true, 512, JSON_THROW_ON_ERROR) :
            [];

        parent::__construct(array_merge(
            $_GET,
            $_POST,
            $_PUT
        ));
    }
}