<?php

namespace Logger;

use Dotenv\Dotenv;

class Environment extends FilteredMap
{
    private static $instance;

    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(__DIR__ .'/../');
        $dbConfig = $dotenv->load();
        parent::__construct($dbConfig);
    }

    public static function getInstance(): Environment
    {
        if (self::$instance == null) {
            self::$instance = new Environment();
        }
        return self::$instance;
    }
}