<?php

namespace Logger;

use JsonException;

class Router
{
    /** @var Request */
    private $request;

    /** @var Response */
    private $response;

    /**  @var array $routeMap  */
    private $routeMap;

    /** @var string[] $regexPatters  */
    private static $regexPatters = [
        'number' => '\d+',
        'string' => '\w+'
    ];
    /**
     * @var array
     */
    private $variables;

    /**
     * Router constructor.
     *
     * @param Request $request
     * @param Response $response
     * @param string $routes
     * @throws JsonException
     */
    public function __construct(Request $request, Response $response, string $routes)
    {
        $this->request = $request;
        $this->response = $response;
        $this->routeMap = json_decode($routes, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        $path = $this->request->getPath();
        foreach ($this->routeMap as $route => $info) {
            $regexRoute = $this->getRegexRoute($route, $info);

            if (preg_match("@^/$regexRoute$@", $path)) {
                $this->executeController(
                    $route, $path, $info, $this->request, $this->response
                );
            }
        }

        //$this->response->notImplemented();
    }

    private function extractParams(
        string $route,
        string $path
    ): array {
        $params = [];
        $pathParts = explode('/', $path);
        $routeParts = explode('/', $route);
        foreach ($routeParts as $key => $routePart) {
            if (strpos($routePart, ':') === 0) {
                $name = substr($routePart, 1);
                $params[$name] = $pathParts[$key+1];
            }
        }
        return $params;
    }

    private function getRegexRoute(
        string $route,
        array $info
    ): string {

        if (isset($info['params'])) {
            foreach ($info['params'] as $name => $type) {
                $route = str_replace(
                    ':' . $name, self::$regexPatters[$type], $route
                );

            }
        }

        return $route;
    }

    /**
     * @param string $route
     * @param string $path
     * @param array $info
     * @param Request $request
     * @param Response $response
     *
     * @return void
     */
    private function executeController(
        string $route,
        string $path,
        array $info,
        Request $request,
        Response $response
    ): void
    {
        $controllerName = '\Api\Controllers\\' . $info['controller'] . 'Controller';
        $controller = new $controllerName($request, $response);

        if ($request->session()->has('user_id')){
            $customerId = $request->session()->getInt('user_id');
            $controller->setCustomerId($customerId);
        }

        $params = $this->extractParams($route, $path);

        call_user_func_array(
            [$controller, $info['method']], $params
        );
    }
}