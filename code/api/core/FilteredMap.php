<?php

namespace Logger;

use JsonSerializable;

abstract class FilteredMap implements JsonSerializable
{
    /** @var array $map */
    private $map;

    /**
     * FilteredMap constructor.
     *
     * @param array $baseMap
     */
    public function __construct(array $baseMap)
    {
        $this->map = $baseMap;
    }

    public function has(string $name): bool
    {
        return isset($this->map[$name]);
    }
    public function set(string $key, $value): void
    {
        $this->map[$key] = $value;
    }

    public function get(string $name)
    {
        return $this->map[$name] ?? null;
    }

    public function getInt(string $name): int
    {
        return (int) $this->get($name);
    }

    public function getNumber(string $name) : float
    {
        return (float) $this->get($name);
    }

    /**
     * @param string $name
     * @param bool $filter
     *
     * @return string|null
     */
    public function getString(string $name, bool $filter = true): ?string
    {
        $value = (string) $this->get($name);
        return $filter ? addslashes($value) : $value;
    }

    protected function clear(): void
    {
        foreach ($this->map as $key => $value) {
            unset($this->map[$key]);
        }
    }

    public function jsonSerialize()
    {
        return $this->map;
    }

    public function toArray(): array
    {
        return $this->map;
    }
}